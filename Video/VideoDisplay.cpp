#include "stdafx.h"
#include "VideoDisplay.h"

#include "../global.h"
#include "../Tool/StringUtility.h"
#include "../Logger/Logger.h"
#include "../UI/Window/BaseWindow.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")

#define FIELD_OF_VIEW (DirectX::XM_PI / 4.0f)

namespace {
	IDXGIAdapter * getDxgiAdapter() {
		IDXGIFactory * dxgiFactory;
		HRESULT result = CreateDXGIFactory(__uuidof(IDXGIFactory), reinterpret_cast<void **>(&dxgiFactory));
		if (FAILED(result)) {
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create DXGI factory");
		}

		IDXGIAdapter * dxgiAdapter;
		result = dxgiFactory->EnumAdapters(0, &dxgiAdapter);
		if (FAILED(result)) {
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to get DXGI adapter for video card");
		}
		return dxgiAdapter;
	}

	DXGI_RATIONAL selectRefreshRate(IDXGIAdapter * dxgiAdapter, uint32_t width, uint32_t height) {
		IDXGIOutput * dxgiOutput;
		HRESULT result = dxgiAdapter->EnumOutputs(0, &dxgiOutput);
		if (FAILED(result)) {
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to get DXGI adapter primary output");
		}

		unsigned int displayModesCount;
		result = dxgiOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &displayModesCount, nullptr);
		if (FAILED(result)) {
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to get number of display modes");
		}

		std::unique_ptr<DXGI_MODE_DESC[]> displayModes;
		displayModes = std::make_unique<DXGI_MODE_DESC[]>(displayModesCount);

		result = dxgiOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &displayModesCount, displayModes.get());
		if (FAILED(result)) {
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to get display modes");
		}

		for (unsigned int i = 0; i < displayModesCount; i++) {
			DXGI_MODE_DESC const & displayMode = displayModes.get()[i];
			if (displayMode.Width == width && displayMode.Height == height) {
				return displayMode.RefreshRate;
			}
		}
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to get appropriate refresh rate");
	}
};

//void VideoDisplay::createDeviceAndSwapChain(Window & window, bool fullscreen, uint32_t width, uint32_t height) {
void VideoDisplay::createDeviceAndSwapChain(BaseWindow & window, bool fullscreen, uint32_t width, uint32_t height) {
	// IDXGIAdapter * dxgiAdapter = getDxgiAdapter();

	D3D_FEATURE_LEVEL const featureLevel = D3D_FEATURE_LEVEL_11_0;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	swapChainDesc.BufferDesc.Width = width;
	swapChainDesc.BufferDesc.Height = height;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	// swapChainDescriptor.BufferDesc.RefreshRate = selectRefreshRate(dxgiAdapter, width, height);
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = window.getHandle();
	swapChainDesc.Windowed = !fullscreen;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_SEQUENTIAL;
	swapChainDesc.Flags = 0;

	/*
	HRESULT const result = D3D11CreateDeviceAndSwapChain(
		nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, D3D11_CREATE_DEVICE_SINGLETHREADED, &featureLevel, 1, D3D11_SDK_VERSION, &swapChainDesc,
		this->swapChain.GetAddressOf(), this->device.GetAddressOf(), nullptr, this->deviceContext.GetAddressOf());
	*/

	HRESULT result = D3D11CreateDevice(
		nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, D3D11_CREATE_DEVICE_SINGLETHREADED, &featureLevel, 1, D3D11_SDK_VERSION, this->device.GetAddressOf(),
		nullptr, this->deviceContext.GetAddressOf());

	if (FAILED(result))
	{
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create device");
	}

	Microsoft::WRL::ComPtr<IDXGIFactory> dxgiFactory;
	result = CreateDXGIFactory(__uuidof(IDXGIFactory), reinterpret_cast<void **>(dxgiFactory.GetAddressOf()));

	if (FAILED(result))
	{
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to dxgi factory");
	}

	result = dxgiFactory->CreateSwapChain(this->device.Get(), &swapChainDesc, this->swapChain.GetAddressOf());

	if (FAILED(result))
	{
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create swap chain buffer");
	}
}

void VideoDisplay::createRenderTargetView() {
	Microsoft::WRL::ComPtr<ID3D11Texture2D> backBuffer;
	HRESULT result = this->swapChain.Get()->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void **>(backBuffer.GetAddressOf()));
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to retrieve swap chain back buffer");
	}
	result = this->device.Get()->CreateRenderTargetView(backBuffer.Get(), nullptr, this->renderTargetView.GetAddressOf());
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create render target view");
	}
}

void VideoDisplay::createDepthStencilBuffer(uint32_t width, uint32_t height) {
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	depthBufferDesc.Width = width;
	depthBufferDesc.Height = height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;
	HRESULT const result = this->device.Get()->CreateTexture2D(&depthBufferDesc, nullptr, this->depthStencilBuffer.GetAddressOf());
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create stencil buffer");
	}
}

void VideoDisplay::createDepthStencilState() {
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	HRESULT const result = this->device.Get()->CreateDepthStencilState(&depthStencilDesc, this->depthStencilState.GetAddressOf());
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create depth stencil state");
	}
}

void VideoDisplay::createDepthStencilView() {
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Flags = 0;
	depthStencilViewDesc.Texture2D.MipSlice = 0;
	HRESULT const result = this->device.Get()->CreateDepthStencilView(
		this->depthStencilBuffer.Get(), &depthStencilViewDesc, this->depthStencilView.GetAddressOf()
	);
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create depth stencil view");
	}
}

void VideoDisplay::createRasterizerState() {
	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_BACK;
	rasterizerDesc.FrontCounterClockwise = false;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = true;
	rasterizerDesc.ScissorEnable = false;
	rasterizerDesc.MultisampleEnable = false;
	rasterizerDesc.AntialiasedLineEnable = false;
	HRESULT const result = this->device.Get()->CreateRasterizerState(&rasterizerDesc, this->rasterizerState.GetAddressOf());
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create rasterizer state");
	}
}

VideoDisplay::VideoDisplay(BaseWindow & window, bool fullscreen, uint32_t width, uint32_t height, float nearPlane, float farPlane) {
//VideoDisplay::VideoDisplay(Window & window, bool fullscreen, uint32_t width, uint32_t height, float nearPlane, float farPlane) {
	RECT clientRect;
	GetClientRect(window.getHandle(), &clientRect);
	// createDeviceAndSwapChain(window, fullscreen, width, height);
	createDeviceAndSwapChain(window, fullscreen, clientRect.right, clientRect.bottom);
	createRenderTargetView();
	// createDepthStencilBuffer(width, height);
	createDepthStencilBuffer(clientRect.right, clientRect.bottom);
	createDepthStencilState();	
	this->deviceContext.Get()->OMSetDepthStencilState(this->depthStencilState.Get(), 1);
	createDepthStencilView();
	this->deviceContext.Get()->OMSetRenderTargets(1, this->renderTargetView.GetAddressOf(), this->depthStencilView.Get());
	createRasterizerState();
	this->deviceContext.Get()->RSSetState(this->rasterizerState.Get());

	D3D11_VIEWPORT viewport;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	// viewport.Width = (float) width;
	// viewport.Height = (float) height;
	viewport.Width = (float) clientRect.right;
	viewport.Height = (float) clientRect.bottom;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	this->deviceContext.Get()->RSSetViewports(1, &viewport);

	this->projectionMatrix = DirectX::XMMatrixPerspectiveFovLH(FIELD_OF_VIEW, viewport.Width / viewport.Height, nearPlane, farPlane);
	this->orthogonalMatrix = DirectX::XMMatrixOrthographicLH(viewport.Width, viewport.Height, nearPlane, farPlane);
}

VideoDisplay::~VideoDisplay() = default;

ID3D11Device * VideoDisplay::getDevice() {
	return this->device.Get();
}

ID3D11DeviceContext * VideoDisplay::getDeviceContext() {
	return this->deviceContext.Get();
}

DirectX::XMMATRIX VideoDisplay::getProjectionMatrix() const {
	return this->projectionMatrix;
}

DirectX::XMMATRIX VideoDisplay::getOrthogonalMatrix() const {
	return this->orthogonalMatrix;
}

void VideoDisplay::clear(DirectX::XMFLOAT4 backgroundColor) {
	this->deviceContext->ClearRenderTargetView(this->renderTargetView.Get(), reinterpret_cast<float *>(&backgroundColor));
	this->deviceContext->ClearDepthStencilView(this->depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void VideoDisplay::display() {
	this->swapChain.Get()->Present(0, 0);
}
