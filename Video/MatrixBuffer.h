#pragma once

#include <DirectXMath.h>

// MatrixBuffer must match the definition given in the VertexShader.hlsl file
struct MatrixBuffer {
	DirectX::XMMATRIX worldMatrix;
	DirectX::XMMATRIX viewMatrix;
	DirectX::XMMATRIX projectionMatrix;
};
