#pragma once

#include <d3d11.h>
#include <DirectXMath.h>
#include <wrl/client.h>

class BaseWindow;

class VideoDisplay {
private:
	Microsoft::WRL::ComPtr<ID3D11Device> device;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext> deviceContext;
	Microsoft::WRL::ComPtr<IDXGISwapChain> swapChain;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView> renderTargetView;
	Microsoft::WRL::ComPtr<ID3D11Texture2D> depthStencilBuffer;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depthStencilState;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView> depthStencilView;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterizerState;
	DirectX::XMMATRIX projectionMatrix;
	DirectX::XMMATRIX orthogonalMatrix;

	Microsoft::WRL::ComPtr<IDXGISwapChain> swapChain2;

	//void createDeviceAndSwapChain(Window & window, bool fullscreen, uint32_t width, uint32_t height);
	void createDeviceAndSwapChain(BaseWindow & window, bool fullscreen, uint32_t width, uint32_t height);
	void createRenderTargetView();
	void createDepthStencilBuffer(uint32_t width, uint32_t height);
	void createDepthStencilState();
	void createDepthStencilView();
	void createRasterizerState();
	
public:
	// TODO: Add in fullscreen and vsync functionalilty.
	VideoDisplay() = default;
	// Throws std::runtime_error if construction fails.
	// explicit VideoDisplay(Window & window, bool fullscreen, uint32_t width, uint32_t height, float nearPlane, float farPlane);
	explicit VideoDisplay(BaseWindow & window, bool fullscreen, uint32_t width, uint32_t height, float nearPlane, float farPlane);
	~VideoDisplay();

	ID3D11Device * getDevice();
	ID3D11DeviceContext * getDeviceContext();
	DirectX::XMMATRIX getProjectionMatrix() const;
	DirectX::XMMATRIX getOrthogonalMatrix() const;
	void clear(DirectX::XMFLOAT4 backgroundColor);
	void display();
};
