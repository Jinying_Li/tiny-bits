#include "stdafx.h"
#include "global.h"

#include "Logger/Logger.h"
#include "Memory/ObjectPool.h"
#include "Memory/PrivateHeap.h"
#include "Memory/WindowThunk.h"
#include "UI/RawInput.h"
#include "UI/Resource/SharedWindowResourcePack.h"
//#include "UI/Window/Window.h"
#include "UI/Window/WindowClassRegistry.h"

uint32_t cacheAlignment;
uint32_t sectorAlignment;
float dpiX, dpiY;
ATOM atom;

std::unique_ptr<Logger> logger;

std::unique_ptr<SharedWindowResourcePack> resources;
std::unique_ptr<WindowClassRegistry> registry;
std::unique_ptr<RawInput> rawInput;

std::unique_ptr<PrivateHeap> codeHeap;
std::unique_ptr<PrivateHeap> dataHeap;
//std::unique_ptr<ObjectPool<Window, PrivateHeap>> windowPool;
std::unique_ptr<ObjectPool<WindowThunk, PrivateHeap>> thunkPool;

Microsoft::WRL::ComPtr<ID2D1Factory> d2dFactory;
Microsoft::WRL::ComPtr<IDWriteFactory> dwriteFactory;
