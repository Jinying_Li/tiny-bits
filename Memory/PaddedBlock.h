#pragma once

// Allows an arbitrary STL container, i.e. Container<T>, to be able to allocate memory from an object pool, i.e. ObjectPool<MemoryBlock<T>>.
template<class T>
struct alignas(alignof(T)) PaddedBlock {
public:
	char reserved0[sizeof(T)];
	void * reserved1[4];
};
