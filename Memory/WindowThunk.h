#pragma once

#include "../global.h"

// x64 adjustor thunk to route procedure for a window.
struct alignas(DEFAULT_MEMORY_ALIGNMENT) WindowThunk {
public:
#pragma pack(push, 1)
	uint16_t mov0;
	uintptr_t value0;
	uint16_t mov1;
	uintptr_t value1;
	uint16_t jmp;
#pragma pack(pop)

	// explicit WindowThunk(uintptr_t value, uintptr_t offset);

	void * WindowThunk::operator new(size_t size);
	void WindowThunk::operator delete(void * pointer);
};
