#include "WindowThunk.h"

#include "ObjectPool.h"
#include "PrivateHeap.h"

#include "../Tool/StringUtility.h"
#include "../Logger/Logger.h"

/*
#define MOV_RCX	0xB948
#define MOV_RAX 0xB848
#define JMP_RAX 0xE0FF

WindowThunk::WindowThunk(uintptr_t value, uintptr_t offset)
	: mov0(MOV_RCX), value0(value), mov1(MOV_RAX), value1(offset), jmp(JMP_RAX) {
}
*/

void * WindowThunk::WindowThunk::operator new(size_t size) {
	void * const pointer = thunkPool.get()->allocate();
#ifndef NDEBUG
	logger.get()->log("Created WindowThunk " + StringUtility::toHexString((uintptr_t) pointer));
#endif
	return pointer;
}

void WindowThunk::WindowThunk::operator delete(void * pointer) {
#ifndef NDEBUG
	logger.get()->log("Destroyed WindowThunk " + StringUtility::toHexString((uintptr_t) pointer));
#endif
	thunkPool.get()->deallocate(pointer);
}
