#pragma once

#include <cstdint>

class PrivateHeap {
private:
	void * handle;

	friend void swap(PrivateHeap & first, PrivateHeap & second);

	PrivateHeap();
public:
	// Heap is allowed to grow if 'maxSize' is 0; actual available memory is slightly less than 'maxSize' due memory overhead.
	// Throws runtime error if construction fails.
	explicit PrivateHeap(size_t size, size_t maxSize, bool dep, bool serialize);
	PrivateHeap(PrivateHeap const & reference) = delete;
	PrivateHeap(PrivateHeap && reference);
	~PrivateHeap();

	PrivateHeap & operator=(PrivateHeap const & reference) = delete;
	PrivateHeap & operator=(PrivateHeap && reference);
	// Throws runtime error if allocation fails.
	void * allocate(size_t size);
	// Undefined behavior if 'memory' is null.
	bool deallocate(void * memory);
};
