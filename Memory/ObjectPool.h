#pragma once

#include <cstdint>

// Memory in an ObjectPool is allocated and deallocated in blocks, the size and alignment being denoted by Block.
// Any object T can be constructed using a block from ObjectPool<Block> as long as sizeof(T) < sizeof(Block). 
template<class Block, class Heap = PrivateHeap>
class ObjectPool {
private:
	struct alignas(alignof(Block)) MemoryBlock {
		union {
			MemoryBlock * next;
			char unused[sizeof(Block)];
		};
	};
	using FreeList = MemoryBlock *;

	Heap * heap;
	void * baseAddress;
	size_t totalBlocks;
	size_t remainingBlocks;
	FreeList freeList;

	friend void swap(ObjectPool & first, ObjectPool & second) {
		using std::swap;
		swap(first.heap, second.heap);
		swap(first.baseAddress, second.baseAddress);
		swap(first.totalBlocks, second.totalBlocks);
		swap(first.remainingBlocks, second.remainingBlocks);
		swap(first.freeList, second.freeList);
	}

	ObjectPool() : heap(nullptr), totalBlocks(0), remainingBlocks(0) {}
public:
	// Throws std::runtime_error if construction fails.
	explicit ObjectPool(Heap & heap, size_t totalBlocks) : heap(&heap), totalBlocks(totalBlocks), remainingBlocks(totalBlocks) {
		if (this->totalBlocks == 0) {
			this->baseAddress = nullptr;
			return;
		}

		// allocate extra memory for alignment
		this->baseAddress = this->heap->allocate(this->totalBlocks * sizeof(Block) + alignof(Block) - 1);
		this->freeList = reinterpret_cast<FreeList>((uintptr_t) this->baseAddress + (alignof(Block) - 1) & ~(alignof(Block) - 1));

		// chain the memory blocks
		FreeList u = this->freeList + (this->totalBlocks - 1); // last memory block
		FreeList v = nullptr;
		while (u >= this->freeList) {
			u->next = v;
			v = u--;
		}
	}

	ObjectPool(ObjectPool const & reference) = delete;

	ObjectPool(ObjectPool && reference) : ObjectPool() {
		swap(*this, reference);
	}

	~ObjectPool() {
		if (this->heap != nullptr) {
			this->heap->deallocate(this->baseAddress); // 'baseAddress' is valid iff 'heap' is valid
		}
	}

	ObjectPool & operator=(ObjectPool const & reference) = delete;

	ObjectPool & operator=(ObjectPool && reference) {
		swap(*this, reference);
		return *this;
	}

	constexpr size_t getBlockSize() const {
		return sizeof(Block);
	}

	size_t getTotalBlocks() const {
		return this->totalBlocks;
	}

	size_t getRemainingBlocks() const {
		return this->remainingBlocks;
	}

	bool hasFreeBlock() const {
		return this->remainingBlocks > 0;
	}

	// Throws std::bad_alloc when out of memory.
	void * allocate() {
		if (!hasFreeBlock()) {
			throw std::bad_alloc();
		}
		void * const memory = this->freeList;
		this->freeList = this->freeList->next;
		--this->remainingBlocks;
		return memory;
	}

	void deallocate(void * memory) {
		FreeList const freeList = reinterpret_cast<FreeList>(memory);
		freeList->next = this->freeList;
		this->freeList = freeList;
		++this->remainingBlocks;
	}
};
