#pragma once

template<class Block_>
class BlockPool;

// Not compatible with functionality which requires variable-sized allocations.
template<class T, class Block>
class BlockAllocator {
public:
	using value_type = T;
	using pointer = T *;
	using const_pointer = T const *;
	using reference = T &;
	using const_reference = T const &;
	using size_type = size_t;
	using difference_type = ptrdiff_t;

	template<class U>
	struct rebind {
		using other = BlockAllocator<U, Block>;
	};

private:
	template<class U, class Block>
	friend class BlockAllocator;

	static BlockPool<Block> * pool;

public:
	explicit BlockAllocator(BlockPool<Block> & pool) : pool(&pool) {
		static_assert(sizeof(T) <= sizeof(Block), "Error; allocator type is larger than block size");
	}

	BlockAllocator(BlockAllocator const & reference)
		: pool(reference.pool) {
	}

	template<class U>
	BlockAllocator(BlockAllocator<U, Block> const & reference) : pool(reference.pool) {
		static_assert(sizeof(U) <= sizeof(Block), "Error; allocator type is larger than block size");
	}

	~BlockAllocator() = default;

	BlockAllocator & operator=(BlockAllocator const & reference) {
		this->pool = reference.pool;
		return *this;
	}

	pointer address(reference object) const {
		return &object;
	}

	const_pointer address(const_reference object) const {
		return &object;
	}

	pointer allocate(size_type elements) {
		if (elements > 1) {
			throw std::invalid_argument("Error; attempted to do variable sized allocation");
		}
		return (pointer) this->pool->allocate();
	}

	void deallocate(pointer memory, size_type elements) {
		if (elements > 1) {
			throw std::invalid_argument("Error; attempted to do variable sized deallocation");
		}
		this->pool->deallocate(memory);
	}

	size_type max_size() const {
		return this->pool->getBlockSize() * this->pool->getRemainingBlocks();
	}

	template<class U, class... Args>
	void construct(U * object, Args &&... args) {
		new (object) U(std::forward<Args>(args)...);
	}

	template<class U>
	void destroy(U * object) {
		object->~U();
	}
};
