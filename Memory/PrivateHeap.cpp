#include "stdafx.h"
#include "PrivateHeap.h"

#include "../global.h"
#include "../Tool/StringUtility.h"
#include "../Logger/Logger.h"

void swap(PrivateHeap & first, PrivateHeap & second) {
	using std::swap;
	swap(first.handle, second.handle);
}

PrivateHeap::PrivateHeap()
	: handle(nullptr) {
}

PrivateHeap::PrivateHeap(size_t size, size_t maxSize, bool dep, bool serialize) {
	DWORD const flags = (HEAP_CREATE_ENABLE_EXECUTE & ~static_cast<uintmax_t>(dep)) | (HEAP_NO_SERIALIZE & ~static_cast<uintmax_t>(serialize));
	this->handle = HeapCreate(flags, size, maxSize);

	if (this->handle == nullptr)
	{
		throw std::runtime_error("Error " + StringUtility::toHexString(GetLastError()) + "; failed to create private heap");
	}

#ifndef NDEBUG
	logger->log("Created private heap " + StringUtility::toHexString((uintptr_t) this->handle));
#endif
}

PrivateHeap::PrivateHeap(PrivateHeap && reference)
	: PrivateHeap() {
	std::swap(*this, reference);
}

PrivateHeap::~PrivateHeap() {
	bool const success = HeapDestroy(this->handle);

#ifndef NDEBUG
	if (success)
	{
		logger->log("Destroyed private heap " + StringUtility::toHexString((uintptr_t) this->handle));
	}
#endif
}

PrivateHeap & PrivateHeap::operator=(PrivateHeap && reference) {
	swap(*this, reference);
	return *this;
}

void * PrivateHeap::allocate(size_t size) {
	void * const memory = HeapAlloc(this->handle, 0, size);

	if (!memory)
	{
		throw std::runtime_error("Error; failed to allocate memory with private heap " + StringUtility::toHexString((uintptr_t) this->handle));
	}

#ifndef NDEBUG
	logger->log("Allocated memory at " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(memory)) + " with private heap " +
		StringUtility::toHexString(reinterpret_cast<uintptr_t>(this->handle)));
#endif

	return memory;
}

bool PrivateHeap::deallocate(void * memory) {
	bool const success = HeapFree(this->handle, 0, memory);

#ifndef NDEBUG
	std::string const message = success ?
		"Deallocated memory at " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(memory)) + " with private heap " +
			StringUtility::toHexString(reinterpret_cast<uintptr_t>(this->handle)) :
		"Error " + StringUtility::toHexString(GetLastError()) + "; failed to deallocate memory at " +
			StringUtility::toHexString(reinterpret_cast<uintptr_t>(memory)) + " with private heap " +
			StringUtility::toHexString(reinterpret_cast<uintptr_t>(this->handle));
	logger->log(message);
#endif

	return success;
}
