#pragma once

#include <Dwrite.h>
#include <wrl/client.h>

class FontFileEnumerator : public IDWriteFontFileEnumerator
{
private:
	volatile unsigned long counter;
	LPCWSTR const * collection;
	UINT32 collectionSize;
	size_t index;
	Microsoft::WRL::ComPtr<IDWriteFactory> factory;

public:
	// Undefined behavior if 'factory' is null.
	// Each string in 'collection' must be the absolute pathname.
	explicit FontFileEnumerator(LPCWSTR const * collection, UINT32 collectionSize, IDWriteFactory * factory);
	FontFileEnumerator(FontFileEnumerator const & reference) = delete;
private:
	~FontFileEnumerator();

public:
	FontFileEnumerator operator=(FontFileEnumerator const & reference) = delete;

	ULONG AddRef() override;
	HRESULT QueryInterface(REFIID riid, void ** ppvObject) override;
	ULONG Release() override;

	HRESULT GetCurrentFontFile(IDWriteFontFile ** fontFile) override;
	HRESULT MoveNext(BOOL * hasCurrentFile) override;
};
