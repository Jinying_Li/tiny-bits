#pragma once

#include <Dwrite.h>

class FontCollectionLoader : public IDWriteFontCollectionLoader
{
private:
	volatile unsigned long counter;

public:
	FontCollectionLoader();
	FontCollectionLoader(FontCollectionLoader const & reference) = delete;
private:
	~FontCollectionLoader();

public:
	FontCollectionLoader & operator=(FontCollectionLoader const & reference) = delete;

	ULONG AddRef() override;
	HRESULT QueryInterface(REFIID riid, void ** ppvObject) override;
	ULONG Release() override;

	HRESULT CreateEnumeratorFromKey(IDWriteFactory * factory, void const * collectionKey, UINT32 collectionKeySize, IDWriteFontFileEnumerator ** fontFileEnumerator);
};
