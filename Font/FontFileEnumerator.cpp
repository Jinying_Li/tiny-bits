#include "stdafx.h"
#include "FontFileEnumerator.h"

#include "../global.h"
#include "../Logger/Logger.h"
#include "../Tool/StringUtility.h"
#include "../Tool/Unicode.h"

FontFileEnumerator::FontFileEnumerator(LPCWSTR const * collection, UINT32 collectionSize, IDWriteFactory * factory)
	: counter(0), collection(collection), collectionSize(collectionSize), index(-1), factory(factory)
{
#ifndef NDEBUG
	logger.get()->log("Created font file enumerator " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(this)));
#endif
}

FontFileEnumerator::~FontFileEnumerator() = default;

ULONG FontFileEnumerator::AddRef()
{
	return _InterlockedIncrement(&this->counter);
}

HRESULT FontFileEnumerator::QueryInterface(REFIID riid, void ** ppvObject)
{
	if (ppvObject == nullptr)
	{
		return E_POINTER;
	}

	if (riid == IID_IUnknown || riid == __uuidof(IDWriteFontFileEnumerator))
	{
		*ppvObject = this;
		AddRef();
		return S_OK;
	}

	*ppvObject = nullptr;
	return E_NOINTERFACE;
}

ULONG FontFileEnumerator::Release()
{
	unsigned long const counter = _InterlockedDecrement(&this->counter);

	if (counter == 0)
	{
		delete this;

#ifndef NDEBUG
		logger.get()->log("Destroyed font file enumerator " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(this)));
#endif
	}

	return counter;
}

HRESULT FontFileEnumerator::GetCurrentFontFile(IDWriteFontFile ** fontFile)
{
	// CreateFontFileReference checks if 'fontFile' is null.
	/*
	if (fontFile == nullptr)
	{
		return E_POINTER;
	}
	*/
	// Calling GetCurrentFontFile(IDWriteFontFile **) before MoveNext(BOOL *) should and will result in undefined behavior.
	// This is the documented behavior of IDWriteFontFileEnumerator by MSDN.
	return this->factory->CreateFontFileReference(this->collection[this->index], nullptr, fontFile);
}

HRESULT FontFileEnumerator::MoveNext(BOOL * hasCurrentFile)
{
	if (hasCurrentFile == nullptr)
	{
		return E_POINTER;
	}

	// No need to also check if 'this->index' is (size_t)-1 since it will always be larger than 'this->collectionSize.'
	// The size of 'this->pathnames' can never reach (size_t)-1, even if sizeof(size_t) == sizeof(uint32_t) since a std::bad_alloc will be thrown beforehand.
	*hasCurrentFile = this->index + 1 < this->collectionSize;
	this->index += *hasCurrentFile;
	return S_OK;
}
