#include "stdafx.h"
#include "FontCollectionLoader.h"

#include "FontFileEnumerator.h"
#include "../global.h"
#include "../Logger/Logger.h"
#include "../Tool/StringUtility.h"
#include <vector>

FontCollectionLoader::FontCollectionLoader()
	: counter(0)
{
#ifndef NDEBUG
	logger.get()->log("Created font collection loader " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(this)));
#endif
}

FontCollectionLoader::~FontCollectionLoader() = default;

ULONG FontCollectionLoader::AddRef()
{
	return _InterlockedIncrement(&this->counter);
}

HRESULT FontCollectionLoader::QueryInterface(REFIID riid, void ** ppvObject)
{
	if (ppvObject == nullptr)
	{
		return E_POINTER;
	}

	if (riid == IID_IUnknown || riid == __uuidof(IDWriteFontCollectionLoader))
	{
		*ppvObject = this;
		AddRef();
		return S_OK;
	}

	*ppvObject = nullptr;
	return E_NOINTERFACE;
}

ULONG FontCollectionLoader::Release()
{
	unsigned long const counter = _InterlockedDecrement(&this->counter);

	if (counter == 0)
	{
		delete this;

#ifndef NDEBUG
		logger.get()->log("Destroyed font collection loader " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(this)));
#endif
	}

	return counter;
}

HRESULT FontCollectionLoader::CreateEnumeratorFromKey(IDWriteFactory * factory, void const * collectionKey, UINT32 collectionKeySize, IDWriteFontFileEnumerator ** fontFileEnumerator)
{
	if (factory == nullptr || collectionKey == nullptr || fontFileEnumerator == nullptr)
	{
		return E_POINTER;
	}

	try
	{
		*fontFileEnumerator = new FontFileEnumerator(reinterpret_cast<LPCWSTR const *>(collectionKey), collectionKeySize / sizeof(void *), factory);
	}
	catch (std::bad_alloc const &)
	{
		return E_OUTOFMEMORY;
	}

	(*fontFileEnumerator)->AddRef();
	return S_OK;
}
