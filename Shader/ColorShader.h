#pragma once

#include <d3d11.h>
#include <DirectXMath.h>
#include <wrl/client.h>

class ColorShader {
private:
	Microsoft::WRL::ComPtr<ID3D11PixelShader> pixelShader;
	Microsoft::WRL::ComPtr<ID3D11VertexShader> vertexShader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout> vertexLayout;
	Microsoft::WRL::ComPtr<ID3D11Buffer> matrixBuffer;

	void createVertexLayout(ID3D11Device & device);
	void createBuffer(ID3D11Device & device);

public:
	explicit ColorShader(ID3D11Device & device);
	~ColorShader();

	void render(ID3D11DeviceContext & deviceContext, uint32_t indexCount,
		DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix);
};
