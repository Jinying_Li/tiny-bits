#include "stdafx.h"
#include "ColorShader.h"

#include "ColorPixelShader.h"
#include "ColorVertexShader.h"
#include "../global.h"
#include "../Tool/StringUtility.h"
#include "../Video/MatrixBuffer.h"

#define VERTEX_ELEMENTS 2
#define VERTEX_POSITION_INDEX 0
#define VERTEX_COLOR_INDEX 1

void ColorShader::createVertexLayout(ID3D11Device & device) {
	// Setup needs to match the Vertex stucture in the shader.
	D3D11_INPUT_ELEMENT_DESC vertexLayoutDesc[VERTEX_ELEMENTS];
	vertexLayoutDesc[VERTEX_POSITION_INDEX].SemanticName = "POSITION";
	vertexLayoutDesc[VERTEX_POSITION_INDEX].SemanticIndex = 0;
	vertexLayoutDesc[VERTEX_POSITION_INDEX].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	vertexLayoutDesc[VERTEX_POSITION_INDEX].InputSlot = 0;
	vertexLayoutDesc[VERTEX_POSITION_INDEX].AlignedByteOffset = 0;
	vertexLayoutDesc[VERTEX_POSITION_INDEX].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	vertexLayoutDesc[VERTEX_POSITION_INDEX].InstanceDataStepRate = 0;
	vertexLayoutDesc[VERTEX_COLOR_INDEX].SemanticName = "COLOR";
	vertexLayoutDesc[VERTEX_COLOR_INDEX].SemanticIndex = 0;
	vertexLayoutDesc[VERTEX_COLOR_INDEX].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	vertexLayoutDesc[VERTEX_COLOR_INDEX].InputSlot = 0;
	vertexLayoutDesc[VERTEX_COLOR_INDEX].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	vertexLayoutDesc[VERTEX_COLOR_INDEX].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	vertexLayoutDesc[VERTEX_COLOR_INDEX].InstanceDataStepRate = 0;
	HRESULT const result = device.CreateInputLayout(
		vertexLayoutDesc, VERTEX_ELEMENTS,
		colorVertexShaderByteCode, sizeof(colorVertexShaderByteCode),
		this->vertexLayout.GetAddressOf()
	);
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create vertex layout");
	}
}

void ColorShader::createBuffer(ID3D11Device & device) {
	D3D11_BUFFER_DESC matrixBufferDesc;
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBuffer);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	HRESULT const result = device.CreateBuffer(&matrixBufferDesc, nullptr, this->matrixBuffer.GetAddressOf());
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create matrix buffer");
	}
}

ColorShader::ColorShader(ID3D11Device & device) {
	HRESULT result = device.CreatePixelShader(colorPixelShaderByteCode, sizeof(colorPixelShaderByteCode), nullptr, this->pixelShader.GetAddressOf());
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create pixel shader");
	}
	result = device.CreateVertexShader(colorVertexShaderByteCode, sizeof(colorVertexShaderByteCode), nullptr, this->vertexShader.GetAddressOf());
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create vertex shader");
	}
	createVertexLayout(device);
	createBuffer(device);
}

ColorShader::~ColorShader() = default;

void ColorShader::render(ID3D11DeviceContext & deviceContext, uint32_t indexCount,
	DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix) {
	worldMatrix = DirectX::XMMatrixTranspose(worldMatrix);
	viewMatrix = DirectX::XMMatrixTranspose(viewMatrix);
	projectionMatrix = DirectX::XMMatrixTranspose(projectionMatrix);

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	HRESULT result = deviceContext.Map(this->matrixBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to map resource");
	}
	MatrixBuffer * data = reinterpret_cast<MatrixBuffer *>(mappedResource.pData);
	data->worldMatrix = worldMatrix;
	data->viewMatrix = viewMatrix;
	data->projectionMatrix = projectionMatrix;
	deviceContext.Unmap(this->matrixBuffer.Get(), 0);
	deviceContext.VSSetConstantBuffers(0, 1, this->matrixBuffer.GetAddressOf());
	deviceContext.IASetInputLayout(this->vertexLayout.Get());
	deviceContext.PSSetShader(this->pixelShader.Get(), nullptr, 0);
	deviceContext.VSSetShader(this->vertexShader.Get(), nullptr, 0);	
	deviceContext.DrawIndexed(indexCount, 0, 0);
}
