#include "stdafx.h"

#include "Logger/Logger.h"
#include "Tool/GraphicUtility.h"
#include "Tool/StringUtility.h"
#include "Tool/Unicode.h"
#include "UI/ProgramClient.h"
#include "UI/RawInput.h"
#include "UI/Resource/SharedWindowResourcePack.h"
#include "UI/Window/WindowClassRegistry.h"
#include "UI/Window/WindowMessage.h"
#include "Memory/ObjectPool.h"
#include "Memory/PrivateHeap.h"
#include "Memory/WindowThunk.h"

#include "Memory/BlockAllocator.h"
#include "Memory/BlockPool.h"
#include "Memory/PaddedBlock.h"

#include <ctime>
#include <Shlwapi.h>
#include <WinIoCtl.h>

#pragma comment(lib, "Shlwapi.lib")

#define TIMESTAMP_FORMAT L"%Y%m%d_%H%M%S"

#define DEFAULT_CURSOR_RES_NAME std::wstring(L"Cursors\\default.ani")
// #define DEFAULT_FONT_RES_NAME std::wstring(L"Fonts\\simkai.ttf")

/*
#define DEFAULT_FONT_SIZE 28
#define DEFAULT_FONT_WEIGHT 500
#define DEFAULT_FONT_TYPE TRUETYPE
#define DEFAULT_FONT_NAME std::wstring(L"����")
*/

std::wstring getTimestamp() {
	wchar_t buffer[16];
	tm timeInfo;
	time_t time = std::time(nullptr);
	localtime_s(&timeInfo, &time);
	wcsftime(buffer, sizeof(buffer), TIMESTAMP_FORMAT, &timeInfo);
	return std::wstring(buffer);
}

void getStorageAlignments() {
	/*
	Before Windows 10, a path cannot exceed 'MAX_PATH' characters by conventional means, however, with Windows 10, it is officially supported that a path can now
	exceed 'MAX_PATH' characters. Thus, the 'MAX_PATH' macro should not be used.
	Considering that most paths will be less than 'MAX_PATH' in length, it seems best to initially predict the length of a path to be less than 256 characters, which
	is the greatest power of 2 less than 'MAX_PATH,' and grow the prediction by a factor of 2 each time the predicted length is insufficient.

	Note that a drive can be mounted as a folder and the approach to derive the drive simply by taking the first few characters and stripping it down to its root is
	not always possible.
	*/
	std::unique_ptr<wchar_t[]> pathname = nullptr;
	uint32_t pathnameLength = 128; // divided by 2 to get 256 for first iteration
	uint32_t copiedLength;
	do {
		pathnameLength *= 2;
		pathname = std::make_unique<wchar_t[]>(pathnameLength);
		copiedLength = GetModuleFileNameW(nullptr, pathname.get(), pathnameLength);
	} while (GetLastError() == ERROR_INSUFFICIENT_BUFFER);
	if (copiedLength == 0) {
		throw std::runtime_error("Error " + StringUtility::toHexString(GetLastError()) + " ; failed to get pathname of current process");
	}

	// Prepend device prefix.
	constexpr wchar_t const DEVICE_PREFIX[] = L"\\\\.\\";
	constexpr size_t DEVICE_PREFIX_LENGTH = sizeof(DEVICE_PREFIX) / sizeof(DEVICE_PREFIX[0]) - 1;
	std::unique_ptr<wchar_t[]> deviceName = std::make_unique<wchar_t[]>(DEVICE_PREFIX_LENGTH + copiedLength + 1);
	wmemcpy(deviceName.get(), DEVICE_PREFIX, DEVICE_PREFIX_LENGTH);
	if (!GetVolumePathNameW(pathname.get(), deviceName.get() + DEVICE_PREFIX_LENGTH, copiedLength + 1)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(GetLastError()) + " ; failed to get volume pathname of current process");
	}

	// Remove trailing backslash.
	for (size_t i = DEVICE_PREFIX_LENGTH; i < copiedLength; ++i) {
		if (deviceName.get()[i + 1] == L'\0') { // the character before the null terminator will always be a backslash
			deviceName.get()[i] = L'\0';
			break;
		}
	}

	HANDLE const driveHandle = CreateFileW(deviceName.get(), STANDARD_RIGHTS_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, 0, nullptr);
	if (driveHandle == INVALID_HANDLE_VALUE) {
		throw std::runtime_error("Error " + StringUtility::toHexString(GetLastError()) + " ; failed to create drive handle");
	}

	STORAGE_ACCESS_ALIGNMENT_DESCRIPTOR storageDesc;
	STORAGE_PROPERTY_QUERY storageQuery;
	storageQuery.QueryType = PropertyStandardQuery;
	storageQuery.PropertyId = StorageAccessAlignmentProperty;
	bool const success = DeviceIoControl(
		driveHandle, IOCTL_STORAGE_QUERY_PROPERTY,
		&storageQuery, sizeof(storageQuery),
		&storageDesc, sizeof(storageDesc),
		reinterpret_cast<DWORD *>(&copiedLength), nullptr
	);
	uint32_t const error = GetLastError();

	CloseHandle(driveHandle);

	if (!success) {
		throw std::runtime_error("Error " + StringUtility::toHexString(error) + " ; failed to query storage properties for drive of current process");
	}

	sectorAlignment = storageDesc.BytesPerPhysicalSector;
}

bool initialize() {
	// Order matters
	logger = std::make_unique<Logger>(L"dump_" + getTimestamp() + L".log");

	getStorageAlignments();

	/*
	bool const success = AddFontResourceExW(DEFAULT_FONT_RES_NAME.data(), FR_PRIVATE, nullptr);
	if (success) {
		logger.get()->log("Loaded default font resource " + Unicode::toUtf8(DEFAULT_FONT_RES_NAME) + " into system");
	}
	*/

	resources = std::make_unique<SharedWindowResourcePack>(
		DEFAULT_CURSOR_RES_NAME//,
		//FontResourceInfo(DEFAULT_FONT_SIZE, DEFAULT_FONT_WEIGHT, false, false, false, DEFAULT_FONT_TYPE, DEFAULT_FONT_NAME.data())
	);

	registry = std::make_unique<WindowClassRegistry>();
	rawInput = std::make_unique<RawInput>();

	atom = registry.get()->add(LOCAL, L"LOCAL_WINDOW", DefWindowProcW);

	codeHeap = std::make_unique<PrivateHeap>((sizeof(WindowThunk) * WINDOW_POOL_SIZE), 0, false, false);
	//dataHeap = std::make_unique<PrivateHeap>((sizeof(Window) * WINDOW_POOL_SIZE), 0, true, false);

	//windowPool = std::make_unique<ObjectPool<Window, PrivateHeap>>(*dataHeap.get(), WINDOW_POOL_SIZE);
	thunkPool = std::make_unique<ObjectPool<WindowThunk, PrivateHeap>>(*codeHeap.get(), WINDOW_POOL_SIZE);

	d2dFactory = GraphicUtility::createD2DFactory(false);
	d2dFactory->GetDesktopDpi(&dpiX, &dpiY);
	dwriteFactory = GraphicUtility::createDWriteFactory();

	return true;
}

void release() {
	// Order matters
	dwriteFactory = nullptr;
	d2dFactory = nullptr;

	//windowPool = nullptr;
	thunkPool = nullptr;

	dataHeap = nullptr;
	codeHeap = nullptr;

	rawInput = nullptr;
	registry = nullptr;
	resources = nullptr;

	/*
	bool success = RemoveFontResourceExW(DEFAULT_FONT_RES_NAME.data(), FR_PRIVATE, nullptr);
	if (success) {
		logger.get()->log("Unloaded default font resource " + Unicode::toUtf8(DEFAULT_FONT_RES_NAME) + " from system");
	}
	*/

	logger = nullptr;
}

std::unique_ptr<ProgramClient> createProgramClient(unsigned int width, unsigned int height) {
	std::unique_ptr<ProgramClient> client = nullptr;
	try {
		client = std::make_unique<ProgramClient>(false, width, height, 300);
	} catch (std::runtime_error & err) {
		logger.get()->log(err.what());
		return nullptr;
	}
	return client;
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR cmdLine, int cmdShow) {
	// Allocate resources to the global variables
	try {
		initialize();
	} catch (std::runtime_error & err) {
		MessageBoxExW(nullptr, L"Failed to initialize resources", L"Error", 0, 0);
		return -1;
	}

	// TODO: Use a config file to save user-preferred top-level window settings
	std::unique_ptr<ProgramClient> programClient = createProgramClient(1200, 800);
	if (programClient == nullptr) {
		release();
		MessageBoxExW(nullptr, L"Failed to create program client", L"Error", 0, 0);
		return -1;
	}

	bool run = true;
	MSG msg;
	while (run) {
		while (PeekMessageW(&msg, nullptr, 0, 0, PM_REMOVE) && msg.message) {
			switch (msg.message) {
			case WM_INPUT:
				programClient.get()->processInput((HRAWINPUT) msg.lParam);
				break;
			case WM_QUIT:
				run = false;
				break;
			case DRAW_WORLD:
				programClient.get()->simulate();
				break;
			default:
				DispatchMessageW(&msg);
				break;
			}
		}
		programClient.get()->render();
	}

	programClient = nullptr;

	// Deallocate resources from the global variables; we defer object deconstruction by deconstructing the unique_ptr itself
	release();

	return 0;
}
