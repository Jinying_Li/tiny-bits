#pragma once

#include <memory>
#include <wrl/client.h>

// The default alignment for the stack and heap allocations on x64 Windows and Linux.
#define DEFAULT_MEMORY_ALIGNMENT 0x10

#define WINDOW_POOL_SIZE 0x80

#ifdef PATHCCH_MAX_CCH
constexpr uint32_t PATHNAME_MAX_LENGTH = PATHCCH_MAX_CCH;
#else
constexpr unsigned int PATHNAME_MAX_LENGTH = 0x8000;
#endif

constexpr unsigned int DPI_PER_DIP = 96;

struct ID2D1Factory;
struct IDWriteFactory;

struct WindowThunk;
class Logger;
class PrivateHeap;
class RawInput;
class SharedWindowResourcePack;
//class Window;
class WindowClassRegistry;

template<class Block, class Heap>
class ObjectPool;

extern uint32_t cacheAlignment;
extern uint32_t sectorAlignment;
extern float dpiX, dpiY;
extern ATOM atom;

extern std::unique_ptr<Logger> logger;
extern std::unique_ptr<SharedWindowResourcePack> resources;
extern std::unique_ptr<WindowClassRegistry> registry;
extern std::unique_ptr<RawInput> rawInput;

extern std::unique_ptr<PrivateHeap> codeHeap;
extern std::unique_ptr<PrivateHeap> dataHeap;
//extern std::unique_ptr<ObjectPool<Window, PrivateHeap>> windowPool;
extern std::unique_ptr<ObjectPool<WindowThunk, PrivateHeap>> thunkPool;

extern Microsoft::WRL::ComPtr<ID2D1Factory> d2dFactory;
extern Microsoft::WRL::ComPtr<IDWriteFactory> dwriteFactory;
