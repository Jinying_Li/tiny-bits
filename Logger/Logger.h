#pragma once

#include <fstream>

class Logger {
private:
	std::ofstream filestream;

public:
	// Throws runtime error if 'logfile' cannot be opened.
	Logger(std::wstring const & logfile);
	Logger(Logger const & reference) = delete;
	~Logger();

	Logger& operator=(Logger const & reference) = delete;
	void log(std::string const & message);
};
