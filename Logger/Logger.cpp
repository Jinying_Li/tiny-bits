#include "Logger.h"

#include "../Tool/Unicode.h"

Logger::Logger(std::wstring const & logfile)
	: filestream(logfile) {
	if (!this->filestream.is_open()) {
		throw std::runtime_error("Failed to open file " + Unicode::toUtf8(logfile));
	}
}

Logger::~Logger() = default;

void Logger::log(std::string const & message) {
	this->filestream.write(message.data(), message.length()) << std::endl;
}
