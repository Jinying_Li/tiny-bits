R&D project for drawing and simulating 3D models with DirectX 11.

Note that the following headers are missing and require to be compiled from HLSL code:

* ColorPixelShader.h from ColorPixelShader.hlsl with global 'colorPixelShaderByteCode'
* ColorVertexShader.h from ColorVertexShader.hlsl with global 'colorVertexShaderByteCode'
