#pragma once

#include <DirectXMath.h>

// Focus vector = (0, 0, 1)
// Up vector = (0, 1, 0)
class Camera {
private:
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT3 rotation;
	DirectX::XMMATRIX viewMatrix;

public:
	explicit Camera(DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), DirectX::XMFLOAT3 rotation = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));

	DirectX::XMFLOAT3 getPosition() const;
	DirectX::XMFLOAT3 getRotation() const;
	void setOrientation(DirectX::XMFLOAT3 const & position, DirectX::XMFLOAT3 const & rotation);
	DirectX::XMMATRIX getViewMatrix() const;
};
