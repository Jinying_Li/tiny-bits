#include "Camera.h" 

#include <algorithm>

Camera::Camera(DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 rotation) {
	setOrientation(position, rotation);
}

DirectX::XMFLOAT3 Camera::getPosition() const {
	return this->position;
}

DirectX::XMFLOAT3 Camera::getRotation() const {
	return this->rotation;
}

void Camera::setOrientation(DirectX::XMFLOAT3 const & position, DirectX::XMFLOAT3 const & rotation) {
	this->position = position;
	this->rotation = rotation;
	DirectX::XMVECTOR const positionVector = XMLoadFloat3(&this->position);
	// DirectX::XMMatrixRotationRollPitchYawFromVector uses radians and NOT degrees despite what MSDN implies.
	DirectX::XMMATRIX const rotationMatrix = DirectX::XMMatrixRotationRollPitchYawFromVector(XMLoadFloat3(&this->rotation));
	DirectX::XMFLOAT3 const focus = DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f);
	DirectX::XMFLOAT3 const up = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
	DirectX::XMVECTOR focusVector = XMLoadFloat3(&focus);
	DirectX::XMVECTOR upVector = XMLoadFloat3(&up);

	focusVector = DirectX::XMVectorAdd(positionVector, DirectX::XMVector3TransformCoord(focusVector, rotationMatrix));
	upVector = DirectX::XMVector3TransformCoord(upVector, rotationMatrix);
	this->viewMatrix = DirectX::XMMatrixLookAtLH(positionVector, focusVector, upVector);
}

DirectX::XMMATRIX Camera::getViewMatrix() const {
	return this->viewMatrix;
}
