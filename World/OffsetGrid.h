#pragma once

#include <cstdint>
#include <d3d11.h>
#include <wrl/client.h>

class OffsetGrid {
private:
	uint32_t width, height;
	uint32_t vertexCount;
	uint32_t indexCount;
	Microsoft::WRL::ComPtr<ID3D11Buffer> vertexBuffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer> indexBuffer;

public:
	explicit OffsetGrid(ID3D11Device & device, uint32_t width, uint32_t height);
	~OffsetGrid();

	void render(ID3D11DeviceContext & deviceContext);
	uint32_t getIndexCount() const;
};
