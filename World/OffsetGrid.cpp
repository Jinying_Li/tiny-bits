#include "stdafx.h"
#include "OffsetGrid.h"

#include "../Tool/StringUtility.h"
#include "../Video/Vertex.h"
#include <memory>

OffsetGrid::OffsetGrid(ID3D11Device & device, uint32_t width, uint32_t height)
	: width(width), height(height) {
	this->vertexCount = this->width * this->height * 8;
	this->indexCount = this->vertexCount;

	std::unique_ptr<Vertex[]> const vertices = std::make_unique<Vertex[]>(this->vertexCount);
	std::unique_ptr<uint32_t[]> const indices = std::make_unique<uint32_t[]>(this->indexCount);
	uint32_t index = 0;
	for (uint32_t i = 0; i < height; ++i) {
		float const offset = i & 1 ? 0.5f : 0.0f;
		for (uint32_t j = 0; j < width; ++j) {
			vertices.get()[index].position = DirectX::XMFLOAT3((float) j + offset, 0.0f, (float) i);
			vertices.get()[index].color = DirectX::XMFLOAT4(0.0f, 0.8f, 0.0f, 1.0f);
			indices.get()[index] = index++;
			vertices.get()[index].position = DirectX::XMFLOAT3((float) j + 1 + offset, 0.0f, (float) i);
			vertices.get()[index].color = DirectX::XMFLOAT4(0.0f, 0.8f, 0.0f, 1.0f);
			indices.get()[index] = index++;

			vertices.get()[index].position = DirectX::XMFLOAT3((float) j + 1 + offset, 0.0f, (float) i);
			vertices.get()[index].color = DirectX::XMFLOAT4(0.0f, 0.8f, 0.0f, 1.0f);
			indices.get()[index] = index++;
			vertices.get()[index].position = DirectX::XMFLOAT3((float) j + 1 + offset, 0.0f, (float) i + 1);
			vertices.get()[index].color = DirectX::XMFLOAT4(0.0f, 0.8f, 0.0f, 1.0f);
			indices.get()[index] = index++;

			vertices.get()[index].position = DirectX::XMFLOAT3((float) j + 1 + offset, 0.0f, (float) i + 1);
			vertices.get()[index].color = DirectX::XMFLOAT4(0.0f, 0.8f, 0.0f, 1.0f);
			indices.get()[index] = index++;
			vertices.get()[index].position = DirectX::XMFLOAT3((float) j + offset, 0.0f, (float) i + 1);
			vertices.get()[index].color = DirectX::XMFLOAT4(0.0f, 0.8f, 0.0f, 1.0f);
			indices.get()[index] = index++;

			vertices.get()[index].position = DirectX::XMFLOAT3((float) j + offset, 0.0f, (float) i + 1);
			vertices.get()[index].color = DirectX::XMFLOAT4(0.0f, 0.8f, 0.0f, 1.0f);
			indices.get()[index] = index++;
			vertices.get()[index].position = DirectX::XMFLOAT3((float) j + offset, 0.0f, (float) i);
			vertices.get()[index].color = DirectX::XMFLOAT4(0.0f, 0.8f, 0.0f, 1.0f);
			indices.get()[index] = index++;
		}
	}

	D3D11_SUBRESOURCE_DATA vertexData;
	vertexData.pSysMem = vertices.get();
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;
	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * this->vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;
	HRESULT result = device.CreateBuffer(&vertexBufferDesc, &vertexData, this->vertexBuffer.GetAddressOf());
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create vertex buffer");
	}

	D3D11_SUBRESOURCE_DATA indexData;
	indexData.pSysMem = indices.get();
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;
	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(uint32_t) * this->indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;
	result = device.CreateBuffer(&indexBufferDesc, &indexData, this->indexBuffer.GetAddressOf());
	if (FAILED(result)) {
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create index buffer");
	}
}

OffsetGrid::~OffsetGrid() = default;

void OffsetGrid::render(ID3D11DeviceContext & deviceContext) {
	uint32_t const stride = sizeof(Vertex);
	uint32_t const offset = 0;
	deviceContext.IASetVertexBuffers(0, 1, this->vertexBuffer.GetAddressOf(), &stride, &offset);
	deviceContext.IASetIndexBuffer(this->indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	deviceContext.IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
}

uint32_t OffsetGrid::getIndexCount() const {
	return this->indexCount;
}
