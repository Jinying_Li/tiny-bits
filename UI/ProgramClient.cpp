#include "stdafx.h"
#include "ProgramClient.h"

#include "RawInput.h"
#include "Window/ButtonWindow.h"
#include "Window/TopLevelWindow.h"
#include "../Font/FontCollectionLoader.h"
#include "../Font/FontFileEnumerator.h"
#include "../Logger/Logger.h"
#include "../Shader/ColorShader.h"
#include "../Tool/StringUtility.h"
#include "../Tool/Unicode.h"
#include "../Video/VideoDisplay.h"
#include "../World/Camera.h"
#include "../World/OffsetGrid.h"
#include <algorithm>
#include <experimental/filesystem>

#pragma comment(lib, "dwrite.lib")

std::wstring const PROGRAM_NAME(L"TINYBITS");

float const CAMERA_X_MIN = 5.0f;
float const CAMERA_X_MAX = 95.0f;
float const CAMERA_Y_MIN = 5.0f;
float const CAMERA_Y_MAX = 10.0f;
float const CAMERA_Z_MIN = CAMERA_X_MIN;
float const CAMERA_Z_MAX = CAMERA_X_MAX;

float const CAMERA_PITCH_MIN = DirectX::XM_PI / 6.0f;
float const CAMERA_PITCH_MAX = DirectX::XM_PI / 3.0f;

namespace {
	// Creates and returns ownership of a TopLevelWindow, also sets 'windowWidth' and 'windowHeight' to the actual width and height of the window.
	std::unique_ptr<TopLevelWindow> createTopLevelWindow(ATOM atom, bool fullscreen, unsigned int width, unsigned int height,
		unsigned int * windowWidth, unsigned int * windowHeight) {
		int const screenWidth = GetSystemMetrics(SM_CXSCREEN);
		int const screenHeight = GetSystemMetrics(SM_CYSCREEN);
		int x, y;

		if (fullscreen)
		{
			*windowWidth = screenWidth;
			*windowHeight = screenHeight;
			x = 0;
			y = 0;
		}
		else
		{
			*windowWidth = width;
			*windowHeight = height;
			x = (screenWidth - *windowWidth) / 2;
			y = (screenHeight - *windowHeight) / 2;
		}

		constexpr float FONT_PIXEL_SIZE = 18.0f;

		float const fontHeight = FONT_PIXEL_SIZE / dpiY * DPI_PER_DIP;
		Microsoft::WRL::ComPtr<IDWriteFontCollection> systemFontCollection;
		dwriteFactory->GetSystemFontCollection(systemFontCollection.GetAddressOf(), false);
		Microsoft::WRL::ComPtr<IDWriteTextFormat> textFormat;
		dwriteFactory->CreateTextFormat(L"Segoe", systemFontCollection.Get(), DWRITE_FONT_WEIGHT_MEDIUM, DWRITE_FONT_STYLE_NORMAL,
			DWRITE_FONT_STRETCH_SEMI_CONDENSED, fontHeight, L"en-US", textFormat.GetAddressOf());
			
		return std::make_unique<TopLevelWindow>(atom, PROGRAM_NAME, x, y, *windowWidth, *windowHeight, !fullscreen, std::make_unique<WindowThunk>(),
			textFormat.Get());
	}

	HWND getWindowHandleFromPoint(HWND parentWindowHandle, POINT screenPoint) {
		unsigned int const GET_WINDOW_HANDLE_FLAGS = CWP_SKIPDISABLED | CWP_SKIPINVISIBLE | CWP_SKIPTRANSPARENT;
		POINT clientPoint = screenPoint;
		ScreenToClient(parentWindowHandle, &clientPoint);
		HWND const windowHandle = ChildWindowFromPointEx(parentWindowHandle, clientPoint, GET_WINDOW_HANDLE_FLAGS);
		if (windowHandle == nullptr || windowHandle == parentWindowHandle) {
			return windowHandle;
		}
		return getWindowHandleFromPoint(windowHandle, screenPoint);
	}

	template<class T>
	T const & bound(T const & value, T const & min, T const & max) {
		return std::min(max, std::max(min, value));
	}

	Microsoft::WRL::ComPtr<IDWriteFontCollection> loadFontCollection()
	{
		class FontCollectionLoaderRegistrar
		{
		private:
			Microsoft::WRL::ComPtr<IDWriteFactory> factory;
			Microsoft::WRL::ComPtr<IDWriteFontCollectionLoader> fontCollectionLoader;

		public:
			FontCollectionLoaderRegistrar(IDWriteFactory * factory)
				: factory(factory)
			{
				this->fontCollectionLoader = Microsoft::WRL::ComPtr<IDWriteFontCollectionLoader>(new FontCollectionLoader);
				HRESULT const result = this->factory->RegisterFontCollectionLoader(this->fontCollectionLoader.Get());

				if (FAILED(result))
				{
					throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to register font collection loader");
				}
			}

			~FontCollectionLoaderRegistrar()
			{
				this->factory->UnregisterFontCollectionLoader(this->fontCollectionLoader.Get());
			}

			IDWriteFontCollectionLoader * getFontCollectionLoader()
			{
				return this->fontCollectionLoader.Get();
			}
		};

		constexpr wchar_t const * fontFileCollection[1]
		{
			L"Fonts\\simkai.ttf"
		};
		constexpr uint32_t collectionSize = sizeof(fontFileCollection) / sizeof(fontFileCollection[0]);

		/*
		Passing 'key' off to COM interface.
		'key' should be a native type so that its data can be passed properly even if the class implementing the COM interface is not C++ code.
		*/
		wchar_t * key[collectionSize];
		// Use 'pathnameCollection' to auto clean up memory.
		std::unique_ptr<wchar_t[]> pathnameCollection[collectionSize];

		/*
			While the current approach is slower than processing and saving the absolute pathnames while simultaneously computing the total memory allocation size
			needed, it uses less than half the memory overhead.
			Computing the total memory allocation size first by processing but not saving the absolute pathnames will result in roughly the same memory overhead but
			would require a second processing of the absolute pathnames. Any difference in speed is negligent in practical cases.
		*/
		for (size_t i = 0; i < collectionSize; ++i)
		{
			std::wstring absolutePathname;

			try
			{
				absolutePathname = std::experimental::filesystem::absolute(fontFileCollection[i]);
			}
			catch (std::experimental::filesystem::filesystem_error const & err)
			{
				throw std::runtime_error("Error " + StringUtility::toHexString(err.code().value()) + "; failed to get absolute path for " +
					Unicode::toUtf8(fontFileCollection[i]));
			}

			pathnameCollection[i] = std::make_unique<wchar_t[]>(absolutePathname.length() + 1);
			wmemcpy(pathnameCollection[i].get(), absolutePathname.c_str(), absolutePathname.length() + 1);
			key[i] = pathnameCollection[i].get();
		}

		FontCollectionLoaderRegistrar fontCollectionLoaderRegistrar = FontCollectionLoaderRegistrar(dwriteFactory.Get());
		Microsoft::WRL::ComPtr<IDWriteFontCollection> fontCollection;
		HRESULT const result = dwriteFactory->CreateCustomFontCollection(fontCollectionLoaderRegistrar.getFontCollectionLoader(), key, sizeof(key),
			fontCollection.GetAddressOf());

		if (FAILED(result))
		{
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create font collection");
		}

#ifndef NDEBUG
		logger->log("Loaded " + std::to_string(fontCollection->GetFontFamilyCount()) + " unique fonts");
#endif

		return fontCollection;
	}
};

// Since camera movements are done per frame, a time interval since the last camera update is needed to ensure camera movements are time dependent.
void ProgramClient::updateCamera(float interval) {
	float const CAMERA_MAX_SPEED = 1.0f;
	float const CAMERA_TRANSLATION_RATE = 0.008f;
	float const CAMERA_ROTATION_RATE = 0.001f;

	float const translationCoefficient = interval * CAMERA_TRANSLATION_RATE;
	float const rotationCoefficient = interval * CAMERA_ROTATION_RATE;

	DirectX::XMFLOAT3 cameraPosition = this->camera.getPosition();
	DirectX::XMFLOAT3 cameraRotation = this->camera.getRotation();
	
	RECT clientRectangle;
	GetClientRect(this->topLevelWindow->getHandle(), &clientRectangle);
	// Normalize dx and dz.
	float const dx = bound((float) this->mouseHandler.getDeltaX() / (clientRectangle.right / 2 + clientRectangle.right % 2), -CAMERA_MAX_SPEED, CAMERA_MAX_SPEED);
	float const dz = bound((float) this->mouseHandler.getDeltaZ() / (clientRectangle.bottom / 2 + clientRectangle.bottom % 2), -CAMERA_MAX_SPEED, CAMERA_MAX_SPEED);
	float const sinYaw = std::sinf(cameraRotation.y);
	float const cosYaw = std::cosf(cameraRotation.y);

	cameraRotation.x = bound(cameraRotation.x + this->mouseHandler.getDeltaYaw() * rotationCoefficient, CAMERA_PITCH_MIN, CAMERA_PITCH_MAX);
	cameraRotation.y = cameraRotation.y - this->mouseHandler.getDeltaPitch() * rotationCoefficient;
	cameraPosition.x = bound(cameraPosition.x + (cosYaw * dx + sinYaw * dz) * translationCoefficient, CAMERA_X_MIN, CAMERA_X_MAX);
	cameraPosition.y = bound(cameraPosition.y + this->mouseHandler.getDeltaY() * translationCoefficient, CAMERA_Y_MIN, CAMERA_Y_MAX);
	cameraPosition.z = bound(cameraPosition.z + (-sinYaw * dx + cosYaw * dz) * translationCoefficient, CAMERA_Z_MIN, CAMERA_Z_MAX);
	this->camera.setOrientation(cameraPosition, cameraRotation);
}

ProgramClient::ProgramClient(bool fullscreen, unsigned int width, unsigned int height, unsigned int fpsLimit)
	: fullscreen(fullscreen), fpsLimit(fpsLimit), frameCount(0), fontCollection(loadFontCollection()),
	topLevelWindow(createTopLevelWindow(atom, this->fullscreen, width, height, &this->width, &this->height))
{
	displayHomeMenu();
	rawInput.get()->registerMouse(this->fullscreen);
	ShowWindow(this->topLevelWindow->getHandle(), SW_SHOW);
}

ProgramClient::~ProgramClient() = default;

void ProgramClient::displayHomeMenu() {
	std::wstring selections[]
	{
		std::wstring(L"風雲再起"),
		std::wstring(L"浮生若夢"),
		std::wstring(L"寒風起心似灰"),
	};

	int x = 100;
	int y = 50;
	constexpr unsigned int w = 300;
	constexpr unsigned int h = 50;

	this->topLevelWindow.get()->clear();

	constexpr float FONT_PIXEL_SIZE = 32.0f;

	float const fontHeight = FONT_PIXEL_SIZE / dpiY * DPI_PER_DIP;
	float const textOffsetX = 20.0f;
	float const textOffsetY = 5.0f;
	Microsoft::WRL::ComPtr<IDWriteTextFormat> textFormat;
	// TODO: Stretch width of font accordingly.
	HRESULT const result = dwriteFactory->CreateTextFormat(L"楷体", this->fontCollection.Get(), DWRITE_FONT_WEIGHT_BOLD, DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL, fontHeight, L"zh-Hant", textFormat.GetAddressOf());


	if (FAILED(result))
	{
		throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create DirectWrite text format");
	}

	for (size_t i = 0; i < sizeof(selections) / sizeof(selections[0]); ++i)
	{
		y += 100;
		std::unique_ptr<ButtonWindow> window;

		try
		{
			// window = std::make_unique<ButtonWindow<int>>(atom, selections[i], x, y, w, h);
			window = std::make_unique<ButtonWindow>(atom, x, y, w, h, std::make_unique<WindowThunk>(), textFormat.Get(), std::move(selections[i]), textOffsetX,
				textOffsetY);
		}
		catch (std::bad_alloc const &)
		{
			throw std::runtime_error("Error; window pool has insufficient memory");
		}

		ButtonWindow * child = static_cast<ButtonWindow *>(this->topLevelWindow->addChild(std::move(window)));
		ShowWindow(child->getHandle(), SW_SHOW);
	}
}

void ProgramClient::processInput(HRAWINPUT inputHandle) {
	RAWINPUT input;
	uint32_t size = sizeof(input);
	if (GetRawInputData(inputHandle, RID_INPUT, &input, &size, sizeof(RAWINPUTHEADER)) == 0) {
		throw std::runtime_error("Error " + StringUtility::toHexString(GetLastError()) + "; failed to get raw input data");
	}
	POINT cursorPosition;
	GetCursorPos(&cursorPosition);
	ScreenToClient(this->topLevelWindow.get()->getHandle(), &cursorPosition);
	RECT clientRectangle;
	GetClientRect(this->topLevelWindow.get()->getHandle(), &clientRectangle);
	this->mouseHandler.processAction(cursorPosition, clientRectangle, input.data.mouse);
}

void ProgramClient::simulate() {
	DirectX::XMFLOAT3 const CAMERA_DEFAULT_POSITION(50.0f, 7.5f, 50.0f);
	DirectX::XMFLOAT3 const CAMERA_DEFAULT_ROTATION(CAMERA_PITCH_MIN, 0.0f, 0.0f);
	this->topLevelWindow.get()->clear();

	try
	{
		this->videoDisplay = VideoDisplay(*this->topLevelWindow, false, 1280, 800, 0.1f, 1000.0f);
		this->camera = Camera(CAMERA_DEFAULT_POSITION, CAMERA_DEFAULT_ROTATION);
		this->grid = std::make_unique<OffsetGrid>(*this->videoDisplay.getDevice(), 100, 100);
		this->colorShader = std::make_unique<ColorShader>(*this->videoDisplay.getDevice());
	}
	catch (std::bad_alloc const &)
	{
		logger.get()->log("Error; window pool has insufficient memory");
	}
	catch (std::runtime_error & err)
	{
		logger.get()->log(err.what());
	}

	this->grid->render(*this->videoDisplay.getDeviceContext());
}

void ProgramClient::render() {
	if (!this->videoDisplay.getDevice())
	{
		return;
	}

	std::chrono::time_point<std::chrono::steady_clock> const currentTime = std::chrono::steady_clock::now();
	std::chrono::duration<float, std::milli> const interval = currentTime - this->previousFrameTime;
	float const fps = 1000 / interval.count();

	if (fps > this->fpsLimit)
	{
		return;
	}

	updateCamera(interval.count());
	this->videoDisplay.clear(DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	this->colorShader.get()->render(*this->videoDisplay.getDeviceContext(), this->grid.get()->getIndexCount(), DirectX::XMMatrixIdentity(),
		this->camera.getViewMatrix(), this->videoDisplay.getProjectionMatrix());
	this->videoDisplay.display();
	// TODO fix the fps computation
	// logger->log(std::to_string(fps));
	this->previousFrameTime = currentTime;
}
