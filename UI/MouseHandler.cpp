#include "stdafx.h"
#include "MouseHandler.h"

#include "../global.h"
#include "../Tool/StringUtility.h"
#include "../Logger/Logger.h"


#define DX 10
#define DY DX

namespace {
	bool isRightButtonDown(uint16_t buttonFlags) {
		return (buttonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN) == RI_MOUSE_RIGHT_BUTTON_DOWN;
	}

	bool isRightButtonUp(uint16_t buttonFlags) {
		return (buttonFlags & RI_MOUSE_RIGHT_BUTTON_UP) == RI_MOUSE_RIGHT_BUTTON_UP;
	}

	bool isScrollWheel(uint16_t buttonFlags) {
		return (buttonFlags & RI_MOUSE_WHEEL) == RI_MOUSE_WHEEL;
	}

	bool isLeftButtonDown(uint16_t buttonFlags) {
		return (buttonFlags & RI_MOUSE_LEFT_BUTTON_DOWN) == RI_MOUSE_LEFT_BUTTON_DOWN;
	}

	bool isLeftButtonUp(uint16_t buttonFlags) {
		return (buttonFlags & RI_MOUSE_LEFT_BUTTON_UP) == RI_MOUSE_LEFT_BUTTON_UP;
	}
}

MouseHandler::MouseHandler()
	: toggleRotation(false), deltaPitch(0), deltaYaw(0), deltaX(0), deltaY(0), deltaZ(0)
{
}

MouseHandler::~MouseHandler() = default;

int32_t MouseHandler::getDeltaPitch() {
	int32_t const deltaPitch = this->deltaPitch;
	this->deltaPitch = 0;
	return deltaPitch;
}

int32_t MouseHandler::getDeltaYaw() {
	int32_t const deltaYaw = this->deltaYaw;
	this->deltaYaw = 0;
	return deltaYaw;
}

// Since 'this->deltaX' is determined based on cursor position, if the cursor is not moved there will not be a new message to set 'this->deltaX' again.
// Thus, getting 'this->deltaX' should not reset it to 0.
int32_t MouseHandler::getDeltaX() {
	return this->deltaX;
}

int32_t MouseHandler::getDeltaY() {
	int32_t const deltaY = this->deltaY;
	this->deltaY = 0;
	return deltaY;
}

// Same as 'getDeltaX()'
int32_t MouseHandler::getDeltaZ() {
	return this->deltaZ;
}

/*
void MouseHandler::process(HWND windowHandle, POINT cursorPosition, RAWMOUSE const & input) {

	ScreenToClient(windowHandle, &cursorPosition);
	LPARAM parameter = (WORD) cursorPosition.y << 16 | (WORD) cursorPosition.x;
	switch (input.usFlags) {
	case MOUSE_MOVE_RELATIVE:
	case MOUSE_MOVE_ABSOLUTE:
	case MOUSE_VIRTUAL_DESKTOP:
		this->deltaCameraZoom = zoomCamera(input.usButtonFlags) ? this->deltaCameraZoom + (int16_t) input.usButtonData : 0;

		if (enableCameraRotation(input.usButtonFlags)) {
			this->toggleRotation = true;
		} else if (disableCameraRotation(input.usButtonFlags)) {
			this->toggleRotation = false;
			this->deltaRotation.x = 0;
			this->deltaRotation.y = 0;
		}
		if (this->lastCursorPosition.x == cursorPosition.x && this->lastCursorPosition.y == cursorPosition.y) {
			break;
		}
		if (this->toggleRotation) {
			this->deltaRotation.x += cursorPosition.x - this->lastCursorPosition.x;
			this->deltaRotation.y += cursorPosition.y - this->lastCursorPosition.y;
		}

		this->lastCursorPosition = cursorPosition;
		if (this->lastWindowHandle != windowHandle) { // cursor moved into a different window
			this->lastWindowHandle = windowHandle;
			SendMessageW(this->lastWindowHandle, WM_SETCURSOR, (WPARAM) windowHandle, HTCLIENT);
		}


		if (leftButtonDown(input.usButtonFlags)) {
			this->activeWindowHandle = windowHandle;
			SendMessageW(this->activeWindowHandle, WM_LBUTTONDOWN, 0, parameter);
		} else if (leftButtonUp(input.usButtonFlags)) {
			if (this->activeWindowHandle == windowHandle) {
				SendMessageW(this->activeWindowHandle, WM_LBUTTONUP, 0, parameter);
			}
			this->activeWindowHandle = nullptr;
		}
		SendMessageW(this->lastWindowHandle, WM_SETCURSOR, (WPARAM) windowHandle, HTCLIENT);
		SendMessageW(this->lastWindowHandle, WM_MOUSEMOVE, 0, parameter);

		break;
	default: // MOUSE_ATTRIBUTES_CHANGED
		return;
	}
}
*/

void MouseHandler::processAction(POINT const & position, RECT const & clientRectangle, RAWMOUSE const & input) {
	switch (input.usFlags) {
	case MOUSE_MOVE_RELATIVE:
		break;
	case MOUSE_MOVE_ABSOLUTE:
	case MOUSE_VIRTUAL_DESKTOP:
		// TODO: Can raw mouse input ever actually use absolute or virtual desktop coordinates?
		return;
	default: // MOUSE_ATTRIBUTES_CHANGED
		return;
	}

	if (position.x <= 0 || position.x >= clientRectangle.right - 1 || position.y <= 0 || position.y >= clientRectangle.bottom - 1) {
		this->deltaX = position.x - (clientRectangle.right / 2 + clientRectangle.right % 2);
		this->deltaZ = -position.y + (clientRectangle.bottom / 2 + clientRectangle.bottom % 2);
		this->toggleRotation = false;
		return;
	}
	this->deltaX = 0;
	this->deltaZ = 0;

	this->deltaY = isScrollWheel(input.usButtonFlags) ? this->deltaY + (int16_t) input.usButtonData : 0;

	if (isRightButtonDown(input.usButtonFlags)) {
		this->toggleRotation = true;
	} else if (isRightButtonUp(input.usButtonFlags)) {
		this->toggleRotation = false;
	}

	if (this->toggleRotation) {
		this->deltaPitch += input.lLastX;
		this->deltaYaw += input.lLastY;
	} else {
		this->deltaPitch = 0;
		this->deltaYaw = 0;
	}
}
