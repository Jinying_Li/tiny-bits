#include "stdafx.h"
#include "RawInput.h"

#include "Window/BaseWindow.h"
#include "../global.h"
#include "../Tool/StringUtility.h"
#include "../Logger/Logger.h"

#define MOUSE_USAGE_PAGE 0x1
#define MOUSE_USAGE 0x2
//#define MOUSE_REGISTER_FLAGS RIDEV_NOLEGACY | RIDEV_EXINPUTSINK)
#define FULLSCREEN_MOUSE_REGISTER_FLAGS RIDEV_NOLEGACY
#define WINDOWED_MOUSE_REGISTER_FLAGS 0

#define MOUSE_DEREGISTER_FLAGS RIDEV_REMOVE

RawInput::~RawInput() {
	deregisterMouse();
}

bool RawInput::registerMouse(BaseWindow & window) {
	RAWINPUTDEVICE inputDevice;
	inputDevice.usUsagePage = MOUSE_USAGE_PAGE;
	inputDevice.usUsage = MOUSE_USAGE;
	inputDevice.dwFlags = WINDOWED_MOUSE_REGISTER_FLAGS;
	inputDevice.hwndTarget = window.getHandle();
	bool const success = RegisterRawInputDevices(&inputDevice, 1, sizeof(RAWINPUTDEVICE));
#ifndef NDEBUG
	std::string const message = success ? "Registered mouse input" : "Error " + StringUtility::toHexString(GetLastError()) + "; failed to register mouse input";
	logger.get()->log(message);
#endif
	return success;
}

bool RawInput::registerMouse(bool fullscreen) {
	RAWINPUTDEVICE inputDevice;
	inputDevice.usUsagePage = MOUSE_USAGE_PAGE;
	inputDevice.usUsage = MOUSE_USAGE;
	inputDevice.dwFlags = fullscreen ? RIDEV_NOLEGACY : 0;
	inputDevice.hwndTarget = nullptr;
	return RegisterRawInputDevices(&inputDevice, 1, sizeof(RAWINPUTDEVICE));
}

bool RawInput::deregisterMouse() {
	RAWINPUTDEVICE inputDevice;
	inputDevice.usUsagePage = MOUSE_USAGE_PAGE;
	inputDevice.usUsage = MOUSE_USAGE;
	inputDevice.dwFlags = MOUSE_DEREGISTER_FLAGS;
	inputDevice.hwndTarget = nullptr;
	bool const success = RegisterRawInputDevices(&inputDevice, 1, sizeof(RAWINPUTDEVICE));
#ifndef NDEBUG
	std::string const message = success ? "Deregistered mouse input" : "Error " + StringUtility::toHexString(GetLastError()) + "; failed to deregister mouse input";
	logger.get()->log(message);
#endif
	return success;
}
