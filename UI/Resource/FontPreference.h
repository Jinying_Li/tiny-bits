#pragma once

enum FontPreference {
	DEFAULT,
	RASTER,
	DEVICE,
	TRUETYPE,
	OUTLINE,
	TRUETYPE_ONLY,
	POSTSCRIPT_ONLY
};
