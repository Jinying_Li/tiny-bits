#pragma once

class SharedResource {
public:
	SharedResource() {}
	virtual ~SharedResource() {}

	virtual void * getData() const = 0;
};
