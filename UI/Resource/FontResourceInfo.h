#pragma once

#include "FontPreference.h"
#include <stdint.h>

struct FontResourceInfo {
public:
	uint32_t size;
	uint32_t weight;
	uint8_t italic;
	uint8_t underline;
	uint8_t strikeout;
	FontPreference preference;
	wchar_t name[32];

	FontResourceInfo(unsigned int size, unsigned int weight, bool italic, bool underline, bool strikeout, FontPreference preference, wchar_t const * name);
};
