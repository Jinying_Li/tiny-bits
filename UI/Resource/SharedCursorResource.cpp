#include "stdafx.h"
#include "SharedCursorResource.h"

#include "../../global.h"
#include "../../Tool/StringUtility.h"
#include "../../Tool/Unicode.h"
#include "../../Logger/Logger.h"

#include <stdexcept>

SharedCursorResource::SharedCursorResource(std::wstring const & name)
	// Note that the LR_SHARED flag is not used.
	: data ((void *) LoadImageW(nullptr, name.data(), IMAGE_CURSOR, 0, 0, LR_DEFAULTSIZE | LR_LOADFROMFILE)) {
	if (this->data == nullptr) {
		throw std::runtime_error("Error " + StringUtility::toHexString(GetLastError()) + "; failed to create cursor handle for " + Unicode::toUtf8(name));
	}
#ifndef NDEBUG
	logger.get()->log("Created cursor handle " + StringUtility::toHexString((uintptr_t) this->data) + " for " + Unicode::toUtf8(name));
#endif
}

SharedCursorResource::~SharedCursorResource() {
	// DestroyCursor must be called since the LR_SHARED flag was not specified for the resource.
	bool const success = DestroyCursor((HCURSOR) this->data);
#ifndef NDEBUG
	if (success) {
		logger.get()->log("Destroyed cursor handle " + StringUtility::toHexString((uintptr_t) this->data));
	}
#endif
}

void * SharedCursorResource::getData() const {
	return this->data;
}
