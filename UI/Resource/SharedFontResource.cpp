#include "stdafx.h"
#include "SharedFontResource.h"

#include "../../global.h"
#include "../../Tool/StringUtility.h"
#include "../../Tool/Unicode.h"
#include "../../Logger/Logger.h"

namespace {
	int32_t getFontPrecision(FontPreference preference) {
		switch (preference) {
		case RASTER:
			return OUT_RASTER_PRECIS;
		case DEVICE:
			return OUT_DEVICE_PRECIS;
		case TRUETYPE:
			return OUT_TT_PRECIS;
		case OUTLINE:
			return OUT_OUTLINE_PRECIS;
		case POSTSCRIPT_ONLY:
			return OUT_PS_ONLY_PRECIS;
		case TRUETYPE_ONLY:
			return OUT_TT_ONLY_PRECIS;
		}
		return OUT_DEFAULT_PRECIS;
	}
}

SharedFontResource::SharedFontResource(FontResourceInfo const & info) {
	this->data = CreateFontW(
		info.size, 0, 0, 0,
		info.weight, info.italic, info.underline, info.strikeout,
		ANSI_CHARSET, getFontPrecision(info.preference), CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY,
		FF_DONTCARE | DEFAULT_PITCH, info.name
	);
	if (this->data == nullptr) {
		throw std::runtime_error("Error " + StringUtility::toHexString(GetLastError()) + "; failed to create font handle for " + Unicode::toUtf8(std::wstring(info.name)));
	}
#ifndef NDEBUG
	logger.get()->log("Created font handle " + StringUtility::toHexString((uintptr_t) this->data));
#endif
}

SharedFontResource::~SharedFontResource() {
	bool const success = DeleteObject((HGDIOBJ) this->data);
#ifndef NDEBUG
	if (success) {
		logger.get()->log("Destroyed font handle " + StringUtility::toHexString((uintptr_t) this->data));
	}
#endif
}

void * SharedFontResource::getData() const {
	return this->data;
}