#pragma once

#include "FontResourceInfo.h"
#include <memory>
#include <string>

class SharedResource;
class SharedCursorResource;
class SharedFontResource;

class SharedWindowResourcePack {
private:
	std::unique_ptr<SharedCursorResource> defaultCursorResource;
	std::unique_ptr<SharedFontResource> defaultFontResource;

public:
	// SharedWindowResourcePack(std::wstring const & defaultCursorResourceName, FontResourceInfo const & defaultFontResourceInfo);
	explicit SharedWindowResourcePack(std::wstring const & defaultCursorResourceName);
	SharedWindowResourcePack(SharedWindowResourcePack const & reference) = delete;
	~SharedWindowResourcePack();

	SharedWindowResourcePack & operator=(SharedWindowResourcePack const & reference) = delete;
	SharedResource const * getDefaultCursorResource() const;
	void setDefaultCursorResource(std::unique_ptr<SharedCursorResource> resource);
	//SharedResource const * getDefaultFontResource() const;
	//void setDefaultFontResource(std::unique_ptr<SharedFontResource> resource);
};
