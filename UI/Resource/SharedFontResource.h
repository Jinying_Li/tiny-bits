#pragma once

#include "FontResourceInfo.h"
#include "SharedResource.h"

class SharedFontResource : public SharedResource {
private:
	void * data;

public:
	// Throws runtime error if object construction fails
	SharedFontResource(FontResourceInfo const & info);
	SharedFontResource(SharedFontResource const & reference) = delete;
	~SharedFontResource();

	SharedFontResource & operator=(SharedFontResource const & reference) = delete;

	void * getData() const;
};
