#include "stdafx.h"
#include "FontResourceInfo.h"

#include "../../global.h"
#include "../../Tool/StringUtility.h"
#include "../../Logger/Logger.h"
#include <cwchar>

FontResourceInfo::FontResourceInfo(unsigned int size, unsigned int weight, bool italic, bool underline, bool strikeout, FontPreference preference,
	wchar_t const * name)
	: size((uint32_t) size), weight((uint32_t) weight), italic((uint8_t) italic), underline((uint8_t) underline), strikeout((uint8_t) strikeout),
	preference(preference) {
	errno_t const error = wcscpy_s(this->name, sizeof(this->name) / sizeof(this->name[0]), name);
	if (error) {
		// An empty string will be copied in case of an error
		logger.get()->log("Error " + StringUtility::toHexString((unsigned long long) error) + "; failed to copy font resource name");
	}
}
