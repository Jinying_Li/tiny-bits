#include "stdafx.h"
#include "SharedWindowResourcePack.h"

#include "SharedCursorResource.h"
#include "SharedFontResource.h"

/*
SharedWindowResourcePack::SharedWindowResourcePack(std::wstring const & defaultCursorResourceName, FontResourceInfo const & defaultFontResourceInfo)
	: defaultCursorResource(std::make_unique<SharedCursorResource>(defaultCursorResourceName)),
		defaultFontResource(std::make_unique<SharedFontResource>(defaultFontResourceInfo)) {
}
*/

SharedWindowResourcePack::SharedWindowResourcePack(std::wstring const & defaultCursorResourceName)
	: defaultCursorResource(std::make_unique<SharedCursorResource>(defaultCursorResourceName))
{
}

SharedWindowResourcePack::~SharedWindowResourcePack() {
	this->defaultCursorResource = nullptr;
	this->defaultFontResource = nullptr;
}

SharedResource const * SharedWindowResourcePack::getDefaultCursorResource() const {
	return this->defaultCursorResource.get();
}

void SharedWindowResourcePack::setDefaultCursorResource(std::unique_ptr<SharedCursorResource> resource) {
	this->defaultCursorResource = std::move(resource);
}

/*
SharedResource const * SharedWindowResourcePack::getDefaultFontResource() const {
	return this->defaultFontResource.get();
}

void SharedWindowResourcePack::setDefaultFontResource(std::unique_ptr<SharedFontResource> resource) {
	this->defaultFontResource = std::move(resource);
}
*/
