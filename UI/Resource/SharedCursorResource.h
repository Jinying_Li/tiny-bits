#pragma once

#include "SharedResource.h"
#include <string>

class SharedCursorResource : public SharedResource {
private:
	void * data;

public:
	// Throws runtime error if object construction fails
	SharedCursorResource(std::wstring const & name);
	SharedCursorResource(SharedCursorResource const & reference) = delete;
	~SharedCursorResource();

	SharedCursorResource & operator=(SharedCursorResource const & reference) = delete;

	void * getData() const;
};
