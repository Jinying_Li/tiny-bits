#include "stdafx.h"
#include "WindowClassRegistry.h"

#include "../../global.h"
#include "../../Tool/StringUtility.h"
#include "../../Tool/Unicode.h"
#include "../../Logger/Logger.h"
#include "../Resource/SharedCursorResource.h"


WindowClassRegistry::~WindowClassRegistry() {
	for (std::map<WindowClass, ATOM>::const_iterator i = this->data.cbegin(); i != this->data.cend(); ) {
		ATOM const atom = i->second;
		bool const success = UnregisterClassW((LPCWSTR) atom, GetModuleHandleW(nullptr));
#ifndef NDEBUG
		std::string const message = success ? "Deregistered ATOM " + StringUtility::toHexString(atom)
			: "Error " + StringUtility::toHexString(GetLastError()) + "; failed to deregister ATOM " + StringUtility::toHexString(atom);
		logger.get()->log(message);
#endif
		i = this->data.erase(i);
	}
}

ATOM WindowClassRegistry::add(WindowClass windowClass, std::wstring const & windowClassName, ProcedureFunction windowProcedure) {
	std::pair<std::map<WindowClass, ATOM>::iterator, bool> temp = this->data.insert(std::pair<WindowClass, ATOM>(windowClass, 0));
	ATOM & atom = temp.first->second;
	if (!temp.second) {
#ifndef NDEBUG
		logger.get()->log("Pre-existing ATOM " + StringUtility::toHexString(atom) + " already exists");
#endif
		return atom;
	}

	WNDCLASSEX entry = {};
	entry.cbSize = sizeof(entry);
	entry.lpfnWndProc = windowProcedure;
	entry.hInstance = GetModuleHandleW(nullptr);
	entry.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	entry.lpszClassName = windowClassName.data();
	atom = RegisterClassExW(&entry);
#ifndef NDEBUG
	std::string const message = atom ? "Registered ATOM " + StringUtility::toHexString(atom) + " for " + Unicode::toUtf8(windowClassName)
		: "Error " + StringUtility::toHexString(GetLastError()) + "; failed to register ATOM " + StringUtility::toHexString(atom);
	logger.get()->log(message);
#endif
	return atom;
}

ATOM WindowClassRegistry::get(WindowClass windowClass) const {
	return this->data.at(windowClass);
}
