#pragma once

#include <list>
#include <memory>
#include <windows.h>

class BaseWindow
{
protected:
	HWND handle;
	BaseWindow const * parent;
	std::list<std::unique_ptr<BaseWindow>>::iterator identifier;
	std::list<std::unique_ptr<BaseWindow>> children;

	explicit BaseWindow(ATOM atom, std::wstring const & name, uint32_t extendedStyleFlags, uint32_t styleFlags, int x, int y, unsigned int width,
		unsigned int height);
public:
	BaseWindow(BaseWindow const & reference) = delete;
	virtual ~BaseWindow();

	BaseWindow & operator=(BaseWindow const & reference) = delete;

	// TODO remove later
	HWND getHandle() const;
	BaseWindow const * getParent() const;
	BaseWindow * addChild(std::unique_ptr<BaseWindow> window);
	std::unique_ptr<BaseWindow> removeChild(BaseWindow * window);
	void clear();
};
