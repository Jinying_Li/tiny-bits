#include "stdafx.h"
#include "ButtonWindow.h"

#include "WindowMessage.h"
#include "../../global.h"
#include "../../Tool/GraphicUtility.h"
#include "../../Tool/StringUtility.h"
#include "../Resource/SharedResource.h"
#include "../Resource/SharedWindowResourcePack.h"
#include <algorithm>

void ButtonWindow::render()
{
	D2D1_SIZE_F const renderTargetSize = this->renderTarget->GetSize();
	D2D1_RECT_F const renderRectangle
	{
		0.0f, 0.0f,
		renderTargetSize.width, renderTargetSize.height
	};
	this->renderTarget->BeginDraw();
	// The size of 'this->bitmap' is always equal to the size 'this->renderTarget'
	this->renderTarget->DrawBitmap(this->bitmap.Get(), &renderRectangle, 1.0f, D2D1_BITMAP_INTERPOLATION_MODE_LINEAR, &renderRectangle);
	this->renderTarget->EndDraw();
}

ButtonWindow::ButtonWindow(ATOM atom, int x, int y, unsigned int width, unsigned int height, std::unique_ptr<WindowThunk> thunk, IDWriteTextFormat * textFormat,
	std::wstring const & text, float textOffsetX, float textOffsetY)
	: WindowTemplate<ButtonWindow>(atom, std::wstring(), 0, WS_POPUP, x, y, width, height, std::move(thunk)), textFormat(textFormat)
{
	this->renderTarget = GraphicUtility::createRenderTarget(d2dFactory.Get(), this->handle, false);
	this->bitmap = GraphicUtility::createBitmap(this->renderTarget.Get(), width, height);
	Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> brush = GraphicUtility::createSolidBrush(this->renderTarget.Get(), D2D1::ColorF(D2D1::ColorF::White));
	D2D1_RECT_F const renderRectangle{textOffsetX, textOffsetY, width - textOffsetX, height - textOffsetY};
	uint32_t const textLength = static_cast<uint32_t>(std::min<size_t>(text.length(), std::numeric_limits<uint32_t>::max()));
	this->renderTarget->BeginDraw();
	this->renderTarget->Clear(D2D1::ColorF(D2D1::ColorF::Gray));
	this->renderTarget->DrawTextW(text.c_str(), textLength, this->textFormat.Get(), renderRectangle, brush.Get());
	HRESULT result = this->renderTarget->EndDraw(); // EndDraw() always succeeds unless 'this->renderTarget' was in an indeterminate state 

	constexpr D2D1_POINT_2U BITMAP_ORIGIN{0, 0};

	D2D1_RECT_U const copyRectangle{0, 0, width, height};
	result = this->bitmap->CopyFromRenderTarget(&BITMAP_ORIGIN, this->renderTarget.Get(), &copyRectangle); // cannot fail
}

ButtonWindow::~ButtonWindow() = default;

LRESULT ButtonWindow::process(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_LBUTTONUP:
		return SendMessageW(this->parent->getHandle(), START, 0, 0);

	case WM_SETCURSOR:
		SetCursor((HCURSOR) resources.get()->getDefaultCursorResource()->getData());
		return TRUE;

	case WM_PAINT:
		render();
		ValidateRect(this->handle, nullptr);
		return 0;

	/*
	case WM_ERASEBKGND:
		return TRUE;
	*/
	}

	return DefWindowProcW(this->handle, message, wParam, lParam);
}
