#pragma once

#include "BaseWindow.h"
#include "../../Memory/WindowThunk.h"

template<class T>
class WindowTemplate : public BaseWindow
{
private:
	friend T;

	std::unique_ptr<WindowThunk> thunk;

	explicit WindowTemplate(ATOM atom, std::wstring const & name, uint32_t extendedStyleFlags, uint32_t styleFlags, int x, int y, unsigned int width,
		unsigned int height, std::unique_ptr<WindowThunk> thunk)
		: BaseWindow(atom, name, extendedStyleFlags, styleFlags, x, y, width, height), thunk(std::move(thunk))
	{
		constexpr uint16_t MOV_RCX = 0xB948;
		constexpr uint16_t MOV_RAX = 0xB848;
		constexpr uint16_t JMP_RAX = 0xE0FF;
		LRESULT (CALLBACK T::* PROCEDURE)(UINT, WPARAM, LPARAM) = &T::process;

		*this->thunk =
		{
			MOV_RCX, reinterpret_cast<uintptr_t>(this),
			MOV_RAX, *reinterpret_cast<uintptr_t *>(&PROCEDURE),
			JMP_RAX
		};

		SetWindowLongPtrW(this->handle, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(this->thunk.get()));
		FlushInstructionCache(GetModuleHandleW(nullptr), nullptr, 0);
	}

public:
	WindowTemplate(WindowTemplate const & reference) = delete;

	~WindowTemplate()
	{
		SetWindowLongPtrW(this->handle, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(DefWindowProcW));
		FlushInstructionCache(GetModuleHandleW(nullptr), nullptr, 0);
	}

	WindowTemplate & operator=(WindowTemplate const & refenrece) = delete;
};
