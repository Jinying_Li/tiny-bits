#include "stdafx.h"
#include "TopLevelWindow.h"

#include "WindowMessage.h"
#include "../../global.h"
#include "../../Tool/GraphicUtility.h"
#include "../../Tool/StringUtility.h"
#include "../Resource/SharedResource.h"
#include "../Resource/SharedWindowResourcePack.h"
#include <algorithm>

constexpr uint32_t WINDOW_STYLE = WS_CAPTION | WS_POPUPWINDOW | WS_MINIMIZEBOX;
constexpr uint32_t NON_WINDOW_STYLE = WS_POPUP;

TopLevelWindow::TopLevelWindow(ATOM atom, std::wstring const & name, int x, int y, unsigned int width, unsigned int height, bool windowed,
	std::unique_ptr<WindowThunk> thunk, IDWriteTextFormat * textFormat)
	: WindowTemplate<TopLevelWindow>(atom, name, WS_EX_APPWINDOW, windowed ? WINDOW_STYLE : NON_WINDOW_STYLE, x, y, width, height, std::move(thunk)),
	textFormat(textFormat)
{
	this->renderTarget = GraphicUtility::createRenderTarget(d2dFactory.Get(), this->handle, false);
	this->brush = GraphicUtility::createSolidBrush(this->renderTarget.Get(), D2D1::ColorF(D2D1::ColorF::White));
}

TopLevelWindow::~TopLevelWindow() = default;

LRESULT CALLBACK TopLevelWindow::process(UINT message, WPARAM wParam, LPARAM lParam)
{
	constexpr WORD WORD_MASK = std::numeric_limits<WORD>::max();

	switch (message)
	{
	case WM_SETCURSOR:
		SetCursor((lParam & WORD_MASK) == HTCLIENT ? (HCURSOR) resources.get()->getDefaultCursorResource()->getData() : LoadCursorW(nullptr, IDC_ARROW));
		return TRUE;

	/*
	case WM_ERASEBKGND:
		return TRUE;
	*/

	case START:
		PostMessageW(nullptr, DRAW_WORLD, 0, 0);
		return 0;

	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProcW(this->handle, message, wParam, lParam);
}

void TopLevelWindow::displayText(std::wstring const & text)
{
	constexpr float OFFSET = 5.0;

	D2D1_SIZE_F const renderTargetSize = this->renderTarget->GetSize();
	D2D1_RECT_F const renderRectangle{OFFSET, OFFSET, renderTargetSize.width - OFFSET, renderTargetSize.height - OFFSET};
	uint32_t const textLength = static_cast<uint32_t>(std::min<size_t>(text.length(), std::numeric_limits<uint32_t>::max()));
	this->renderTarget->BeginDraw();
	this->renderTarget->DrawTextW(text.c_str(), textLength, this->textFormat.Get(), renderRectangle, brush.Get());
	this->renderTarget->EndDraw();
}
