#pragma once

enum WindowType {
	UNDEFINED,
	FULLSCREEN,
	FRAME,
	STATIC,
	BUTTON
};
