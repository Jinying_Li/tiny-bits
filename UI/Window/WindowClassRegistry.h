#pragma once

#include "WindowClass.h"
#include <map>
#include <windows.h>

#define WINDOW_TEXT_MAX_LENGTH 32

typedef LRESULT(CALLBACK *ProcedureFunction)(HWND, UINT, WPARAM, LPARAM);

class WindowClassRegistry {
private:
	std::map<WindowClass, ATOM> data;

public:
	WindowClassRegistry() = default;
	WindowClassRegistry(WindowClassRegistry const & reference) = delete;
	~WindowClassRegistry();
	
	WindowClassRegistry & operator=(WindowClassRegistry const & reference) = delete;

	ATOM add(WindowClass windowClass, std::wstring const & windowClassName, ProcedureFunction windowProcedure);
	ATOM get(WindowClass windowClass) const;
};
