#pragma once

#include <cstdint>

// WindowState can be a combination of any of the following.
enum WindowState : int8_t {
	NONE = 0x0,
	// RAW_INPUT = 0x1,
	UPDATE_STATUS = 0x2
};

inline WindowState operator|(WindowState first, WindowState second) {
	return static_cast<WindowState>(static_cast<int8_t>(first) | static_cast<int8_t>(second));
}

inline WindowState operator&(WindowState first, WindowState second) {
	return static_cast<WindowState>(static_cast<int8_t>(first) & static_cast<int8_t>(second));
}

inline WindowState operator~(WindowState operand) {
	return static_cast<WindowState>(~static_cast<int8_t>(operand));
}
