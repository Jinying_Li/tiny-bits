#include "stdafx.h"
#include "BaseWindow.h"

#include "../../global.h"
#include "../../Logger/Logger.h"
#include "../../Tool/StringUtility.h"

BaseWindow::BaseWindow(ATOM atom, std::wstring const & name, uint32_t extendedStyleFlags, uint32_t styleFlags, int x, int y, unsigned int width,
	unsigned int height)
	: parent(nullptr)
{
	LPCWSTR const className = reinterpret_cast<LPWSTR>(static_cast<uintptr_t>(atom));
	LPCWSTR const windowName = name.c_str();

	this->handle = CreateWindowExW(
		extendedStyleFlags, className, windowName, styleFlags,
		x, y, width, height,
		nullptr, nullptr, GetModuleHandleW(nullptr), nullptr);

	if (this->handle == nullptr)
	{
		throw std::runtime_error("Error " + StringUtility::toHexString(GetLastError()) + "; failed to create window handle");
	}

#ifndef NDEBUG
	logger.get()->log("Created window handle " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(this->handle)));
#endif
}

BaseWindow::~BaseWindow()
{
	this->children.clear();
	bool const success = DestroyWindow(this->handle); // it is okay if 'this->handle' is null

#ifndef NDEBUG
	if (success)
	{
		logger.get()->log("Destroyed window handle " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(this->handle)));
	}
#endif
}

HWND BaseWindow::getHandle() const
{
	return this->handle;
}

BaseWindow const * BaseWindow::getParent() const
{
	return this->parent;
}

BaseWindow * BaseWindow::addChild(std::unique_ptr<BaseWindow> window)
{
	this->children.push_back(std::move(window));
	BaseWindow * added = this->children.back().get();
	// added->identifier = reinterpret_cast<uintptr_t>(&*--this->children.end());
	added->identifier = --this->children.end();
	added->parent = this;
	SetWindowLongPtrW(added->handle, GWL_STYLE, GetWindowLongPtrW(added->handle, GWL_STYLE) & ~WS_POPUP | WS_CHILD);
	SetParent(added->handle, this->handle);
	return added;
}

std::unique_ptr<BaseWindow> BaseWindow::removeChild(BaseWindow * window)
{
	window->parent = nullptr;
	SetParent(window->handle, nullptr);
	SetWindowLongPtrW(window->handle, GWL_STYLE, GetWindowLongPtrW(window->handle, GWL_STYLE) & ~WS_CHILD | WS_POPUP);
	// std::list<std::unique_ptr<BaseWindow>>::iterator iterator = reinterpret_cast<std::list<std::unique_ptr<BaseWindow>>::iterator>(window->identifier);
	std::list<std::unique_ptr<BaseWindow>>::iterator iterator = window->identifier;
	std::unique_ptr<BaseWindow> removed = std::move(*iterator);
	this->children.erase(iterator);
	return removed;
}

void BaseWindow::clear()
{
	this->children.clear();
}




