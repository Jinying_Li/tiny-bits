#pragma once

#include "WindowTemplate.h"
#include <dwrite.h>
#include <wrl/client.h>

class ButtonWindow : public WindowTemplate<ButtonWindow>
{
private:
	Microsoft::WRL::ComPtr<ID2D1Bitmap> bitmap;
	Microsoft::WRL::ComPtr<ID2D1HwndRenderTarget> renderTarget;
	Microsoft::WRL::ComPtr<IDWriteTextFormat> textFormat;
	// std::wstring text;

	void render();

public:
	// Throws std::runtime_error if construction fails.
	explicit ButtonWindow(ATOM atom, int x, int y, unsigned int width, unsigned int height, std::unique_ptr<WindowThunk> thunk, IDWriteTextFormat * textFormat,
		std::wstring const & text, float textOffsetX, float textOffsetY);
	ButtonWindow(ButtonWindow const & reference) = delete;
	~ButtonWindow();

	ButtonWindow & operator=(ButtonWindow const & reference) = delete;
	LRESULT CALLBACK process(UINT message, WPARAM wParam, LPARAM lParam);
};
