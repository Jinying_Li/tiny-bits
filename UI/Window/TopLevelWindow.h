#pragma once

#include "WindowTemplate.h"
#include <dwrite.h>
#include <wrl/client.h>

class TopLevelWindow : public WindowTemplate<TopLevelWindow> {
private:
	Microsoft::WRL::ComPtr<ID2D1HwndRenderTarget> renderTarget;
	Microsoft::WRL::ComPtr<IDWriteTextFormat> textFormat;
	Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> brush;

public:
	// Throws std::runtime_error if construction fails.
	explicit TopLevelWindow(ATOM atom, std::wstring const & name, int x, int y, unsigned int width, unsigned int height, bool windowed,
		std::unique_ptr<WindowThunk> thunk, IDWriteTextFormat * textFormat);
	TopLevelWindow(TopLevelWindow const & reference) = delete;
	~TopLevelWindow();

	TopLevelWindow & operator=(TopLevelWindow const & reference) = delete;
	LRESULT CALLBACK process(UINT message, WPARAM wParam, LPARAM lParam);
	// Throws std::runtime_error if on failure.
	void displayText(std::wstring const & text);
};
