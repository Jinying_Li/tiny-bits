#pragma once

class BaseWindow;

class RawInput {
public:
	RawInput() = default;
	~RawInput();

	// Does nothing if mouse is already registered for raw input.
	bool registerMouse(BaseWindow & window);
	bool registerMouse(bool fullscreen);
	// Does nothing if mouse is not registered for raw input.
	bool deregisterMouse();
};
