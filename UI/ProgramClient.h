#pragma once

#include "MouseHandler.h"
#include "../Video/VideoDisplay.h"
#include "../World/Camera.h"
#include <chrono>
#include <dwrite.h>
#include <memory>
#include <wrl/client.h>

class ColorShader;
class OffsetGrid;
class TopLevelWindow;

class ProgramClient
{
private:
	bool fullscreen;
	unsigned int width, height;
	std::chrono::time_point<std::chrono::steady_clock> previousFrameTime;
	unsigned int fpsLimit;
	unsigned int frameCount;
	Microsoft::WRL::ComPtr<IDWriteFontCollection> fontCollection;
	MouseHandler mouseHandler;
	VideoDisplay videoDisplay;
	Camera camera;
	std::unique_ptr<OffsetGrid> grid;
	std::unique_ptr<ColorShader> colorShader;;
	std::unique_ptr<TopLevelWindow> topLevelWindow;

	void updateCamera(float interval);
	void displayHomeMenu();

	ProgramClient() = default;
public:
	// Throws std::runtime_error if construction fails.
	explicit ProgramClient(bool fullscreen, unsigned int width, unsigned int height, unsigned int fpsLimit);
	ProgramClient(ProgramClient & reference) = delete;
	ProgramClient(ProgramClient && reference) = default;

	ProgramClient & operator=(ProgramClient & reference) = delete;
	ProgramClient & operator=(ProgramClient && reference) = default;

	~ProgramClient();

	void processInput(HRAWINPUT inputHandle);
	// Window * getTopLevelWindow();

	void simulate();
	void render();
};
