#pragma once

#include <cstdint>
#include <windows.h>

class MouseHandler
{
private:
	// HWND activeWindowHandle;
	bool toggleRotation;
	int32_t deltaPitch, deltaYaw;
	int32_t deltaX, deltaY, deltaZ;

public:
	MouseHandler();
	~MouseHandler();

	int32_t getDeltaPitch();
	int32_t getDeltaYaw();
	int32_t getDeltaX();
	int32_t getDeltaY();
	int32_t getDeltaZ();
	// void process(HWND windowHandle, POINT cursorPosition, RAWMOUSE const & input);
	void processAction(POINT const & position, RECT const & windowRectangle, RAWMOUSE const & input);
};
