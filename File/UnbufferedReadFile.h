#pragma once

#include <memory>
#include <string>
#include <windows.h>

class UnbufferedReadFile
{
private:
	std::shared_ptr<void> handle;

public:
	// Throws std::runtime_error if construction fails.
	explicit UnbufferedReadFile(std::wstring const & filename);
	~UnbufferedReadFile();

	HANDLE getHandle() const;
	// Throws std::runtime_error if function fails.
	// Undefined behavior if 'buffer' is null.
	size_t read(char * buffer, size_t length) const;
};
