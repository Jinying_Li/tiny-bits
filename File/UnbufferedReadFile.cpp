#include "stdafx.h"
#include "UnbufferedReadFile.h"

#include "../global.h"
#include "../Logger/Logger.h"
#include "../Tool/StringUtility.h"
#include "../Tool/Unicode.h"
#include <algorithm>

UnbufferedReadFile::UnbufferedReadFile(std::wstring const & filename)
{
	HANDLE const fileHandle = CreateFileW(
		filename.c_str(), GENERIC_READ, FILE_SHARE_READ,
		nullptr, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING | FILE_FLAG_SEQUENTIAL_SCAN,
		nullptr);

	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		throw std::runtime_error("Error " + StringUtility::toHexString(GetLastError()) + "; failed to create file handle for " + Unicode::toUtf8(filename));
	}

#ifndef NDEBUG
	logger.get()->log("Created file handle " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(fileHandle)));
#endif

	this->handle = std::shared_ptr<void>(
		fileHandle,
		[] (HANDLE handle)
		{
			bool const success = CloseHandle(handle);

#ifndef NDEBUG
			if (success)
			{
				logger.get()->log("Destroyed file handle " + StringUtility::toHexString(reinterpret_cast<uintptr_t>(handle)));
			}
#endif
		});
}

UnbufferedReadFile::~UnbufferedReadFile() = default;

HANDLE UnbufferedReadFile::getHandle() const
{
	return this->handle.get();
}

size_t UnbufferedReadFile::read(char * buffer, size_t length) const
{
	DWORD const maxBytesToRead = std::numeric_limits<DWORD>::max() / sectorAlignment * sectorAlignment;
	DWORD bytesRead;
	OVERLAPPED inputInfo = {};
	size_t totalBytesRead = 0;

	do
	{
		DWORD const bytesToRead = static_cast<DWORD>(std::min<size_t>(maxBytesToRead, length)); // overflow not possible since the min is taken

		if (!ReadFile(this->handle.get(), buffer + totalBytesRead, bytesToRead, &bytesRead, &inputInfo))
		{
			throw std::runtime_error(
				"Error " + StringUtility::toHexString(GetLastError()) + " ; failed to read from file handle " +
				StringUtility::toHexString(reinterpret_cast<uintptr_t>(this->handle.get())));
		}

		totalBytesRead += bytesRead;
		length -= bytesRead;
	}
	while (bytesRead != 0 || length > 0);

	return totalBytesRead;
}
