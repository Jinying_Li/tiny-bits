#pragma once

#include "Position.h"

#define BLOCK_SIZE 32

class OffsetSquareBlock {
	Position (*data)[BLOCK_SIZE][BLOCK_SIZE];
};
