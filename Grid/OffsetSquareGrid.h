#pragma once

#include "OffsetSquareBlock.h"

#define GRID_SIZE 512
#define GRID_LENGTH (GRID_SIZE / BLOCK_SIZE)

class OffsetSquareGrid {
	OffsetSquareBlock data[GRID_LENGTH][GRID_LENGTH];
};
