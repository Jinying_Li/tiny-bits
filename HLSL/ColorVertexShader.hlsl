cbuffer MatrixBuffer {
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

struct Vertex {
	float4 position : POSITION;
	float4 color : COLOR;
};

struct Pixel {
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

Pixel GetPixel(Vertex vertex) {
	Pixel pixel;

	// Change the position vector to be 4 units for proper matrix calculations.
	vertex.position.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	pixel.position = mul(vertex.position, worldMatrix);
	pixel.position = mul(pixel.position, viewMatrix);
	pixel.position = mul(pixel.position, projectionMatrix);

	// Store the input color for the pixel shader to use.
	pixel.color = vertex.color;

	return pixel;
}
