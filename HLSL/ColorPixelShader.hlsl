struct Pixel {
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

float4 GetColor(Pixel pixel) : SV_TARGET {
	return pixel.color;
}
