#include "Unicode.h"

namespace Unicode {
	void encode(char* buffer, unsigned short bmp) {
		if (bmp <= 0x7F) {
			buffer[0] = (char) bmp;
			buffer[1] = '\0';
			return;
		}
		if (bmp <= 0x7FF) {
			buffer[0] = (char) ((bmp >> 6) | 0xC0);
			buffer[1] = (char) (bmp & 0x3F | 0x80);
			buffer[2] = '\0';
			return;
		}
		buffer[0] = (char) ((bmp >> 12) | 0xE0);
		buffer[1] = (char) ((bmp >> 6) & 0x3F | 0x80);
		buffer[2] = (char) (bmp & 0x3F | 0x80);
		buffer[3] = '\0';
	}

	void encode(char* buffer, unsigned int sp) {
		buffer[0] = (char) ((sp >> 18) | 0xF0);
		buffer[1] = (char) ((sp >> 12) & 0x3F | 0x80);
		buffer[2] = (char) ((sp >> 6) & 0x3F | 0x80);
		buffer[3] = (char) (sp & 0x3F | 0x80);
		buffer[4] = '\0';
	}

	std::string toUtf8(std::wstring const & utf16) {
		// The UTF-8 string requires 3 times the number of bytes needed to represent the UTF-16 string in the worst case scenario
		std::string utf8;
		utf8.reserve(utf16.length() * 3);

		char buffer[5];
		bool pair = false;
		unsigned short a, b;
		for (wchar_t const & w : utf16) {
			if (!pair) {
				a = (unsigned short) w;
				if (a < 0xD800 || a > 0xDFFF) {
					Unicode::encode(buffer, a);
					utf8.append(buffer);
				} else {
					pair = true;
				}
				continue;
			}
			a = (a - 0xD800) & 0x3FF;
			b = ((unsigned short) w - 0xDC00) & 0x3FF;
			Unicode::encode(buffer, (((unsigned int) a << 10) | b) + 0x10000);
			utf8.append(buffer);
			pair = false;
		}
		return utf8;
	}
}
