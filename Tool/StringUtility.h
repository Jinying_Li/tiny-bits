#pragma once

#include <string>

namespace StringUtility
{
	namespace Helper
	{
		constexpr size_t getLength(size_t accumulated, wchar_t const * string)
		{
			return *string ? getLength(accumulated + 1, string + 1) : accumulated;
		}
	}

	constexpr size_t getLength(wchar_t const * string)
	{
		return Helper::getLength(0, string);
	}

	std::string toHexString(size_t value);
}
