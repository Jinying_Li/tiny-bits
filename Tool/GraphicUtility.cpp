#include "stdafx.h"
#include "GraphicUtility.h"

#include "StringUtility.h"
#include "../global.h"
#include "../Logger/Logger.h"

#pragma comment(lib, "d2d1.lib")

namespace GraphicUtility
{
	constexpr D2D1_PIXEL_FORMAT PIXEL_FORMAT{DXGI_FORMAT_R8G8B8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED};

	Microsoft::WRL::ComPtr<ID2D1Bitmap> createBitmap(ID2D1RenderTarget * renderTarget, uint32_t width, uint32_t height)
	{
		D2D1_SIZE_U const size{width, height};
		D2D1_BITMAP_PROPERTIES const bitmapProperties{PIXEL_FORMAT, dpiX, dpiY};
		Microsoft::WRL::ComPtr<ID2D1Bitmap> bitmap;
		HRESULT const result = renderTarget->CreateBitmap(size, nullptr, 0, bitmapProperties, bitmap.GetAddressOf());

		if (FAILED(result))
		{
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create bitmap");
		}

		return bitmap;
	}

	Microsoft::WRL::ComPtr<ID2D1Factory> createD2DFactory(bool serialize)
	{
#ifndef NDEBUG
		constexpr D2D1_FACTORY_OPTIONS options
		{
			D2D1_DEBUG_LEVEL_INFORMATION
		};
#else
		constexpr D2D1_FACTORY_OPTIONS options
		{
			D2D1_DEBUG_LEVEL_NONE
		};
#endif
		Microsoft::WRL::ComPtr<ID2D1Factory> factory;
		HRESULT const result = D2D1CreateFactory(serialize ? D2D1_FACTORY_TYPE_MULTI_THREADED : D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory), &options,
			reinterpret_cast<void **>(factory.GetAddressOf()));

		if (FAILED(result))
		{
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create Direct2D factory");
		}

		return factory;
	}

	Microsoft::WRL::ComPtr<IDWriteFactory> createDWriteFactory()
	{
		Microsoft::WRL::ComPtr<IDWriteFactory> factory;
		HRESULT result = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown **>(factory.GetAddressOf()));

		if (FAILED(result))
		{
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create DirectWrite factory");
		}

		return factory;
	}

	Microsoft::WRL::ComPtr<ID2D1HwndRenderTarget> createRenderTarget(ID2D1Factory * factory, HWND windowHandle, bool vsync)
	{
		constexpr D2D1_RENDER_TARGET_PROPERTIES RENDER_TARGET_PROPERTIES
		{
			D2D1_RENDER_TARGET_TYPE_DEFAULT,
			PIXEL_FORMAT,
			0.0f,
			0.0f,
			D2D1_RENDER_TARGET_USAGE_NONE,
			D2D1_FEATURE_LEVEL_DEFAULT // the application must support DirectX 11 to run so this field doesn't really matter
		};
		RECT windowRectangle;
		GetClientRect(windowHandle, &windowRectangle);
		D2D1_HWND_RENDER_TARGET_PROPERTIES const hwndRenderTargetProperties
		{
			windowHandle,
			// 'windowRectangle.right' and 'windowRectangle.bottom' will always be positive.
			{static_cast<uint32_t>(windowRectangle.right), static_cast<uint32_t>(windowRectangle.bottom)},
			vsync ? D2D1_PRESENT_OPTIONS_NONE : D2D1_PRESENT_OPTIONS_IMMEDIATELY
		};
		Microsoft::WRL::ComPtr<ID2D1HwndRenderTarget> renderTarget;
		HRESULT const result = factory->CreateHwndRenderTarget(RENDER_TARGET_PROPERTIES, hwndRenderTargetProperties, renderTarget.GetAddressOf());

		if (FAILED(result))
		{
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create D2D render target");
		}

		return renderTarget;
	}

	Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> createSolidBrush(ID2D1RenderTarget * renderTarget, D2D_COLOR_F const & color)
	{
		constexpr D2D1_MATRIX_3X2_F BRUSH_MATRIX
		{
			1.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f
		};
		constexpr D2D1_BRUSH_PROPERTIES BRUSH_PROPERTIES{1.0f, BRUSH_MATRIX};

		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> brush;
		HRESULT const result = renderTarget->CreateSolidColorBrush(color, BRUSH_PROPERTIES, brush.GetAddressOf());

		if (FAILED(result))
		{
			throw std::runtime_error("Error " + StringUtility::toHexString(result) + "; failed to create brush");
		}

		return brush;
	}
}

