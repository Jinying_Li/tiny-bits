#pragma once

#include <cstdint>
#include <wrl/client.h>

struct ID2D1Bitmap;
struct ID2D1Factory;
struct ID2D1HwndRenderTarget;
struct ID2D1SolidColorBrush;
struct IDWriteFactory;

namespace GraphicUtility
{
	// The following functions are throw std::runtime_error on failure.
	Microsoft::WRL::ComPtr<ID2D1Bitmap> createBitmap(ID2D1RenderTarget * renderTarget, uint32_t width, uint32_t height);
	Microsoft::WRL::ComPtr<ID2D1Factory> createD2DFactory(bool serialize);
	Microsoft::WRL::ComPtr<IDWriteFactory> createDWriteFactory();
	Microsoft::WRL::ComPtr<ID2D1HwndRenderTarget> createRenderTarget(ID2D1Factory * factory, HWND windowHandle, bool vsync);
	Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> createSolidBrush(ID2D1RenderTarget * renderTarget, D2D_COLOR_F const & color);
}
