#include "StringUtility.h"

namespace StringUtility {
	std::string toHexString(size_t value) {
		char const HEXADECIMALS[16] = {
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'a', 'b', 'c', 'd', 'e', 'f'
		};

		std::string hexString;
		hexString.reserve((sizeof(size_t) << 1) + 3);
		hexString.append("0x");
		size_t i = (size_t) -1 >> 4;
		for (; i > 0; i = i >> 4) {
			if (value > i) {
				break;
			}
		}
		for (; i > 0; i = i >> 4) {
			size_t const quotient = value / (i + 1);
			value -= (i + 1) * quotient;
			hexString.push_back(HEXADECIMALS[quotient]);
		}
		hexString.push_back(HEXADECIMALS[value]);
		return hexString;
	}

	/*
	constexpr size_t getLength(char const * string) {
		return *string ? 1 + getLength(string + 1) : 0;
	}
	*/
}
