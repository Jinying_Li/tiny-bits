#pragma once

#include <string>

namespace Unicode {
	std::string toUtf8(std::wstring const & utf16);
}
